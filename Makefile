default: main.pdf

.PHONY: fast
fast:
	pdflatex main

main.dvi: */*.tex *.tex Makefile img/* *.bib
	latex main
	biber main
	latex main
	latex main

main.ps: main.dvi
	dvips main.dvi

quietps: *.tex Makefile img/*
	latex -interaction=batchmode main
	latex -interaction=batchmode main
	latex -interaction=batchmode main
	dvips main.dvi

dvi: main.dvi

ps: main.ps

main.pdf: */*.tex *.tex Makefile img/* *.bib
	pdflatex main
	biber main
	pdflatex main
	pdflatex main

pdf: main.pdf

clean: 
	-rm -f *.{log,aux}

dist-clean:
	-rm -f *.{log,aux,dvi,ps,pdf,toc,bbl,blg,slo,srs,out,bak,lot,lof}
	-(cd img; make dist-clean)

all: pdf
