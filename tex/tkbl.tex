In this chapter we introduce a logic that will be used to reason about
knowledge in \tfppf{}. It will also be the base upon which the \texttppl{}
\tppl{} (Chap. \ref{chapter:tppl}) will be built.

Again, we follow in the footsteps of the authors of \fppf{} \cite{ppf},
extending their knowledge-based \kbl{} with two temporal operators and a new
epistemic operator for learning. The resulting logic is called \texttkbl{}, or
\tkbl{}.


\section{Syntax of \texorpdfstring{\tkbl{}}{TKBL}}
\label{sec:formula-structure:t}

We assume that the most basic building blocks of the logic -- the function,
relation, and constant symbols -- are parts of a vocabulary.  We also assume
that the former two have an implicit arity, corresponding to the number of
arguments they take. Furthermore, we assume we have an infinite supply of
variables we can use.

\begin{definition}[Term]
\label{def:term:t}
   Let \( c \) be a constant, \( f \) be a function symbol, and \( x \) be a
   variable. A \emph{term \( s \)} is inductively defined as
    \[
       s ::= c \mid x \mid f(\term{s}),
    \]
    where \( \term{s} \) is a tuple of terms respecting the arity of \( f \).
\end{definition}

One of the parts that sets \tkbl{} (and the original \kbl{}) apart from other
logics is the existence of two special types of predicates: connections and
actions. These mirror the different kinds of edges in the social graph of
\tsnm{}s (Def. \ref{def:tsnm:t}). To recapitulate: Connections \conns{}
represent relationships between agents, as defined by the authors of the
specific \osn{} they are modeling. They can be symmetric (\mword{friends}) as
well as asymmetric (\mword{follower}). Actions (or permissions) \perms{}
connect two users \vaga{}, \vagb{} if \vaga{} is allowed to execute a specific
action towards \vagb{}, for example send a friend request.

\begin{definition}[Syntax of \tkbl{}]
\label{def:syntax-of-tkbl:t}
   Given
   agents \( \vaga, \vagb \in \ag \),
   a nonempty set of agents \( \vgr \subseteq \ag \),
   predicate symbols \( a_n(\vaga, \vagb)\), \( c_m(\vaga, \vagb)\), \(
   p(\vterm) \) where \( m \in \conns \) and \( n \in \perms \),
   and a variable \( x \),
   the \emph{syntax of the \texttkbl{} \tkbl{}} is inductively defined as:
   \begin{align*}
      \phi &::= \rho \mid
                \phi \land \phi \mid
                \neg \phi \mid
                \forall x. \phi \mid
                \knows_\vaga \psi \mid
                \learns_\vaga \psi \mid
                \comknow_\vgr \psi \mid
                \disknow_\vgr \psi \mid
                \al \phi \mid
                \ev \phi
                \\
      \psi &::= \rho \mid
                \psi \land \psi \mid
                \neg \psi \mid
                \forall x. \psi \mid
                \knows_\vaga \psi \mid
                \learns_\vaga \psi \mid
                \comknow_\vgr \psi \mid
                \disknow_\vgr \psi \\
      \rho &::= c_m(\vaga, \vagb) \mid
                a_n(\vaga, \vagb) \mid
                p(\vterm) \\                
   \end{align*}   
\end{definition}

We use \ftkbl{} to denote the set of all well-formed formulae of \tkbl{}.

The epistemic modalities used here are read, in turn: \( \knows_\vaga \psi \)
as ``agent \vaga{} knows \( \psi \)'', \( \learns_\vaga \psi \) as ``agent
\vaga{} learns \( \psi \)'', \( \comknow_\vgr \psi \) as ``it is common
knowledge in group \vgr{} that \( \psi \)'', and \( \disknow_\vgr \psi \) as
''it is distributed knowledge in group \vgr{} that \( \psi \)''. The temporal
operator \al{} is read ``always'', \ev{} is ``eventually''.

There are two main differences between \tkbl{} and \kbl{}.

\paragraph{Temporal Operators} 
\tkbl{} utilizes the temporal operators \al{} and \ev{}. These can be found in
many temporal logics, where they are usually defined using a more basic
\emph{until} operator. We do not have such operator here since we were unable
to find interesting use cases in the context of \osn{}s.

Note also that no temporal operator is allowed inside an epistemic modality.
The reason is that, as we are about to show, the temporal operators are used
with regards to traces, to be able to iterate through them, whereas once a
formula is inside an epistemic modality, it is checked in a single knowledge
base which itself is static.

\paragraph{Learning Modality}
In a static context, the \knows{} modality is quite enough to express that an
agent possesses a specific piece of knowledge. In a dynamic context, however,
we found it more natural to separate \emph{knowing} something and
\emph{learning} it. The distinction is captured by what we later on establish
as the learning axiom: if an agent learns something, she knows it.

Learning can be intuitively described as the instant in time when an agent
comes into contact with a piece of information. On the other hand, knowing
something is not a single point in time, it is more of an interval (Fig.
\ref{fig:knowing-and-learning}).


\begin{figure}
\begin{center}
\begin{tikzpicture}
   \tikzstyle{help lines} = [dotted, gray]
   \tikzstyle{diff} = [font=\small, above=-1, midway]
   \tikzstyle{know} = [very thick, ->]
   \tikzstyle{learn} = [very thick, dashed]
   \tikzstyle{elem} = [circle, inner sep=2pt]

   \draw[style=help lines, step=0.5cm, xshift=-0.0cm] (0.99, 0) grid (6.49, 1.9);

   \draw[|-stealth] (0.99, 0) -- (6.5, 0);
   \foreach \x in {1, 2, 4, 6}
      \draw[shift={(\x, 0)}] (0, 0) -- (0, -3pt) node[below] {\( t_\x \)};

   \draw[style=know, cyan] (1, 0.5) -- (6.5, 0.5) node[style=diff] {\( \knows_\vag \vfb \)};
   \node[style=elem, fill=cyan] (1) at (1, 0.5) {};
   \node[font=\small, cyan] (1) at (1, 0.85) {\( \learns_\vag \vfb \)};

   \draw[style=know, orange] (2, 1.5) -- (6.5, 1.5) node[style=diff] {\( \knows_\vag \vfa \)};
   \node[style=elem, fill=orange] (2) at (2, 1.5) {};
   \node[font=\small, orange] (2) at (2, 1.85) {\( \learns_\vag \vfa \)};

\end{tikzpicture}
\caption[Learning and knowing]
        {Our interpretation of learning and knowing can be illustrated by this
        picture. Suppose we have two formulae \vfa{} and \vfb{} and an agent
        \vag{}. \( t_1 \) and \( t_2 \) represent the point in time when \vag{}
        came to possess (learned) \vfb{} and \vfa{}, respectively. If this
        picture captures all knowledge transfer, then \( \learns_\vag \vfb{} \)
        should only be true at time \( t_1 \) and \( \learns_\vag \vfa{} \)
        only at time \( t_2 \). However, at any time \( t \geq t_1 \) (for
        example \( t_4 \)) we have \( \knows_\vag \vfb \) and at any time \( t'
        \geq t_2 \) (such as \( t_6 \)), \( \knows_\vag \vfa \).}
\label{fig:knowing-and-learning}
\end{center}
\end{figure}


\subsection{Notation}
\label{subsec:tkbl-notation:t}

We will use the following syntactic sugar borrowed from \kbl{}. Given agents \(
\vaga, \vagb \in \ag \), a nonempty group \( \vgr \subseteq \ag \), and an
action \( a_n \), we have:

\begin{center}
\begin{tabular}{rcl}
   \( P^{c}_b a_n \) & \( := \) & \( a_n(b, c) \) \\
   \( SP^{c}_\vgr a_n \) & \( := \) & \( \bigvee_{b \in \vgr} a_n(b, c) \) \\
   \( GP^{c}_\vgr a_n \) & \( := \) & \( \bigwedge_{b \in \vgr} a_n(b, c) \)
\end{tabular}
\end{center}

These stand for, in turn: ``\( b \) is permitted to execute \( a_n \) to
\( c \)'', ``someone in \vgr{} is permitted to execute \( a_n \) to \( c \)'',
and ``everyone in \vgr{} is permitted to execute \( a_n \) to \( c \)''.

Often it is useful to be able to express that, in a group \vgr{}, everyone
(\everyone) knows (or learns) something. The same goes for someone (\someone)
in a group \vgr{} learning or knowing something. These notions are
straightforward to define using the basic \knows{} and \learns{} modalities:

\begin{center}
\begin{tabular}{ll}
   \( \someone_\vgr \vf \triangleq \bigvee_{\vag \in \vgr} \knows_\vag \vf \)
   &
   \( \someone\learns_\vgr \vf \triangleq \bigvee_{\vag \in \vgr} \learns_\vag
   \vf \)\\
   \( \everyone_\vgr \vf \triangleq \bigwedge_{\vag \in \vgr} \knows_\vag \vf
   \)
   &
   \( \everyone\learns_\vgr \vf \triangleq \bigwedge_{\vag \in \vgr} \learns_\vag
   \phi \)
\end{tabular}
\end{center}

These are read ``someone in \vgr{} knows (learns) \vf{}'' and ``everyone in
\vgr{} knows (learns) \vf{}''.


\section{Agents and Their Knowledge}
\label{sec:agents-and-their-knowledge:t}

Let us now retrace a bit and take a closer look at the knowledge bases of
agents, first defined in Def. \ref{def:tsnm:t}. The set retrieved by the \kb{}
function contains everything the agent knows, in form of timestamped \tkbl{}
formulae.

In addition to possessing specific knowledge, we also want to make the agents
smarter by enabling them to gain new knowledge on their own, if it follows from
what they already know. For this purpose we define the notion of
\emph{\texttcl{}} (\tcl{}). In short, the closure of a knowledge base \kb{} of
an agent contains all the knowledge already in \kb{}, plus all knowledge that
can be inferred from it according to a set of rules. Of course, for this to be
useful, the process should be sound -- agents should not be able to infer
something that is not true in the \tsnm{}.

Following is the formal definition of \tcl{} and the related notion of timed
derivation.

\begin{definition}[\textcctcl{}]
\label{def:timed-closure:t}
   Given the knowledge base of an agent \vaga, \( \kb_\vaga \), the \emph{timed
   closure of \( \kb_\vaga \), \tclkba{}}, satisfies the following properties:
   \begin{enumerate}
      \item
         For all \( \vf \in \ftkbl \) and \( \vts \in \tstampset \), if \(
         (\vf, \vts) \in \tclkba \) then \( (\neg \vf, \vts) \not \in \tclkba
         \).
      \item
         Introduction and elimination rules for conjunction:
         \begin{itemize}
            \item[\( \wedge_I \) -]
               If \( (\vfa, \vts) \in \tclkba \) and \( (\vfb, \vts') \in
               \tclkba \), then \( (\vfa \wedge \vfb, \max(\vts, \vts')) \in
               \tclkba \).
            \item[\( \wedge_{E_1} \) -]
               If \( (\vfa \wedge \vfb, \vts) \in \tclkba \) and \( \wedge_I \)
               was not used to derive \( \vfa \wedge \vfb \), then \( (\vfa,
               \vts) \in \tclkba \).
            \item[\( \wedge_{E_2} \) -]
               Analogous to \( \wedge_{E_1} \) but for \( \vfb \).
         \end{itemize}
      \item
         If \( (\vfa, \vts) \in \tclkba \) and \( (\vfa \implies \vfb, \vts')
         \in \tclkba \), then \( (\vfb, \max(\vts, \vts')) \in \tclkba \).
         \label{mp}
      \item
         If \( (\vfa, \vts) \in \tclkba \) then \( (\knows_\vaga \vfa, \vts)
         \in \tclkba \).
      \item
         If \vfa{} is provable in the axiomatization \sfiveaxioma{}
         (\cite{reasoning}) from \tclkba{}, then \( \vfa \in \tclkba \).
         Formally:
         \begin{itemize}
            \item[A1 -]
               If \vf{} is an instance of a first-order tautology, then \(
               (\vf, \tautime) \in \tclkba \).
            \item[A2 -]
               If \( (\knows_\vag \vfa, \vts) \in \tclkba \) and \(
               (\knows_\vaga (\vfa \implies \vfb), \vts') \in \tclkba \), then
               \( (\knows_\vaga \vfb, \max(\vts, \vts')) \in \tclkba \).
            \item[A3 -]
               If \( (\knows_\vaga \vfa, \vts) \in \tclkba \), then \( (\vfa,
               \vts) \in \tclkba \).
            \item[A4 -]
               If \( (\knows_\vaga \vf, \vts) \in \tclkba \), then \(
               (\knows_\vaga \knows_\vaga \vf, \vts) \in \tclkba \).
            \item[A5 -]
               If \( (\vf, \vts) \notin \tclkba \), then \( (\neg \knows_\vag
               \vf, \vts) \in \tclkba \).
            \item[R1 -]
               {\em Modus ponens}, defined as \ref{mp}).
            \item[R2 -]
               If \( (\vf, \vts) \) is provable from no assumptions (i.e.,
               \vf{} is a tautology), then \( (\knows_\vag \vf, \vts) \in
               \tclkba \).
            \item[C1 -]
               \( (\everyone_\vgr \vf, \vts) \in \tclkba \) iff \(
               (\bigwedge_{\viter \in \vgr} \knows_\viter \vf, \vts) \in
               \tclkba \).
            \item[C2 -]
               \( (\comknow_\vgr \vf, \vts) \in \tclkba \) iff \(
               (\everyone_\vgr (\vf \wedge \comknow_\vgr \vf), \vts) \in
               \tclkba \).
            \item[RC1 -]
               If \( (\vfa \implies \everyone_\vgr(\vfb \wedge \vfa), \vts) \)
               is provable from no assumptions, then \( (\vfa \implies
               \comknow_\vgr \vfb, \vts) \in \tclkba \).
            \item[D1 -]
               \( (\disknow_{\{\vaga\}} \vfa, \vts) \in \tclkba \) iff \(
               (\knows_\vaga \vfa, \vts) \in \tclkba \).
            \item[D2 -]
               If \( (\disknow_\vgr \vfa, \vts) \in \tclkba \), then \(
               (\disknow_{\vgr'} \vfa, \vts) \in \tclkba \) if \( \vgr
               \subseteq \vgr' \).
            \item[DA2-DA5]
               Properties A2, A3, A4 and A5, replacing the modality \(
               \knows_\vag \) with the modality \( \disknow_\vgr \) for each
               axiom.
         \end{itemize}
   \end{enumerate}
\end{definition}

\begin{definition}[Timed Derivation]
\label{def:timed-derivation:t}
   A {\em timed derivation} of a formula \( \vf \in \ftkbl \) with timestamp \(
   \vts \in \tstampset \) is a finite sequence of formulae and timestamps
   \[
      (\vfa_1, \vts_1), (\vfa_2, \vts_2), \dots, (\vf_\vnat, \vts_\vnat) =
      (\vfa, \vts),
   \]
   where each \( \vfa_\viter \), for \( 1 \leq \viter \leq \vnat \),
   is either an instance of the axioms or the conclusion of one of the
   derivation rules of which premises has already been derived, i.e., it
   appears as \( \vfa_\viterb \) with \( \viterb < \viter \) of \tclkba.
\end{definition}

The previous definitions build on those defined for the original \fppf{}. The
main difference is the handling of timestamps in knowledge bases of agents.
More precisely, we want to ensure that a timestamp attached to a formula in a
\kb{} has the intended meaning, i.e., it should be the time when that
particular information was either learned or inferred.

\paragraph{Combining Knowledge}
For instance, we allow agents to combine individual pieces of information using
the conjuction introduction rule \( \land_I \). The timestamp of the resulting
conjunction is the maximum of the timestamps of the individual formulae. The
reasoning behind this is that one can claim they know both \vfa{} and \vfb{}
only once they obtain both \vfa{} and \vfb{} -- more precisely, at the point in
time when the last missing piece was obtained. This is also the case in the
\emph{modus ponens} rule in \ref{mp}: the resulting knowledge could only be
inferred at the point when the agent is aware of both the precondition and the
rule itself.

An agent is also capable of breaking a conjunction \( \vf \land \vfb \) down
into its two conjuncts \vfa{} and \vfb{}. This is, however, only possible if it
was not obtained using the introduction rule. The reason for this is to avoid
obtaining knowledge with incorrect timestamps. If the restriction were not in
place and we attempted to break down a conjunction obtained by \( \land_I \),
we could end up with either \vfa{} or \vfb{} with an illegal timestamp.

\paragraph{Tautologies}
Tautologies are a special case as they should be true at any point in time. To
reflect this, we use a special timestamp \tautime{} which stands for any
timestamp (for all \( \vts \in \tstampset \), \( \tautime = \vts \)). This
guarantees that, when using a tautology in a derivation including other
premises, we will delegate the timestamp \vts{} of the premise that is not a
tautology since \( \max(\vts, \tautime) = t \).

\paragraph{Knowledge Evolution}
It should be noted that in our model, the knowledge of agents grows
monotonically -- there is no notion of forgetting and the agents remember
everything they have learned so far. We do not provide a formal proof here, but
it follows from examining Def. \ref{def:timed-closure:t} case by case as none
of the properties results in inferred knowledge with a timestamp less than
those of the premises used. Neither is it the case that inferred knowledge
would get a timestamp strictly greater than the maximum of its premises.


\section{Semantics of \texorpdfstring{\tkbl{}}{TKBL}}
\label{sec:semantics-of-tkbl:t}

Now that we have defined both the syntax of \tkbl{} and the notion of a trace,
we are ready to introduce a precise way to determine whether a formula holds in
a trace.

\begin{definition}[Satisfiability Relation for \tkbl{}]
\label{def:satisfiability-relation:t}
   Given
   a well-formed trace \( \vtrace \in \traceset \),
   agents \( \vaga, \vagb \in \ag_\vtrace \),
   a finite set of agents \( \vgr \subseteq \ag \),
   formulae \( \vfa, \vfb \in \ftkbl \),
   \( m \in \conns \),
   \( n \in \perms \),
   \( o \in \dom \),
   and \( \vts \in \tracets \),
   the \emph{satisfiability relation \sat{}} is defined as shown in Fig.
   \ref{fig:satisfiability-relation:t}.
\end{definition}

\begin{figure}
\begin{center}
  \begin{tabular}{lcl}
    \( \vtrace, \vts \sat \al \vfa \) &
    iff &
    for all \( \vts' \in \tracets \), \( \vts' \geq \vts \), \( \vtrace, \vts'
    \sat \vfa \)\\
    \( \vtrace, \vts \sat \ev \vfa \) &
    iff &
    there exists \( \vts' \in \tracets \), \( \vts' \geq \vts \), \\&& such
    that \( \vtrace, \vts' \sat \vfa \) \\
    \\
    \( \vtrace, \vts \sat \neg \vfa \) &
    iff &
    \( \vtrace, \vts \; \cancel{\sat} \; \vfa \) \\
    \( \vtrace, \vts \sat \vfa \land \vfb \) &
    iff &
    \( \vtrace, \vts \sat \vfa \) and \( \vtrace, \vts \sat \vfb \) \\
    \( \vtrace, \vts \sat \forall x. \vfa \) &
    iff &
    for all \( v \in D_o^{\vtrace[t]} \), \( \vtrace, \vts \sat \vfa[v / x]
    \) \\
    \\
    \( \vtrace, \vts \sat c_m(\vaga, \vagb) \) &
    iff &
    \( (\vaga, \vagb) \in C_m^{\vtrace[t]} \) \\
    \( \vtrace, \vts \sat a_n(\vaga, \vagb) \) &
    iff &
    \( (\vaga, \vagb) \in A_n^{\vtrace[t]} \) \\
    \\
    \( \vtrace, \vts \sat p(\vterm) \) &
    iff &
    there exists \( \vts' \in \tracets \) such that \( (p(\vterm), \vts') \in
    \kb^{\vtrace[t]}_\environ \) \\
    \\
    \( \vtrace, \vts \sat \knows_\vaga \vfa \) &
    iff &
    there exists \( \vts' \in \tracets \) such that \( (\vfa, \vts') \in
    \tcl(\kb^{\vtrace[t]}_\vag) \) \\
    \( \vtrace, \vts \sat \learns_\vag \vfa \) &
    iff &
    \( (\vfa, \vts) \in \tcl(\kb^{\vtrace[t]}_\vag) \) \\
    \\
    \( \vtrace, \vts \sat \comknow_\vgr \vfa \) &
    iff &
    \( \vtrace, \vts \sat \everyone_\vgr^{\vnat} \vfa \) for \( \vnat = 1, 2,
    \ldots \) \\
    \( \vtrace, \vts \sat \disknow_\vgr \vfa \) &
    iff &
    there exists \( \vts' \in \tracets \) such that \( (\vfa, \vts') \in
    \tcl(\bigcup_{\viter \in \vgr}\kb^{\vtrace[\vts]}_\viter) \)
  \end{tabular}
\end{center}
\caption[Satisfiability relation for \tkbl{}]%
        {The semantics for \tkbl{} is given in terms of the satisfiability 
         relation.}
\label{fig:satisfiability-relation:t}
\end{figure}

In the semantics, we use a timestamp \vts{} to help determine which \tsnm{} in
the trace we are interested in. For the temporal operators \al{} and \ev{},
which are used to manipulate \vts{}, this simply means that we have to check
either all \tsnm{}s starting with \vts{}, or we have to find at least one at
time \vts{} or greater for which the inner formula holds.

The cases of negation, conjunction, and quantification are dealt with in a
standard way. For connections and actions, we simply look into the relational
structure of the \tsnm{} at time \vts{} to determine whether the agents in
question were in the relation at that point.

Predicates are checked with respect to the environment, which contains
everything that is true in the \tsnm{}. A predicate is considered to hold at
time \vts{} if the knowledge base of the environment at time \vts{} contains
said predicate with any timestamp (note that since \vtrace{} is well-formed,
only timestamps less than \vts{} and \vts{} itself may exist there). The
knowledge modality \knows{} is treated in essentially the same way, with the
only exception being that we look into the timed closure of the knowledge of
the agent in question. Learning \vf{} at time \vts{} in terms of the semantics
given here means that \vf{} with timestamp \vts{} has to be in the timed
closure of the knowledge base at time \vts{} -- in other words, \vf{} has to
have appeared in the closure at precisely \vts{}.

The semantics given for common knowledge are defined using the \everyone{}
shortcut modality (Sec. \ref{subsec:tkbl-notation:t}). For \vf{} to be
distributed knowledge among agents of \vgr{} at time \vts{} it has to be the
case that \vf{} appears with some timestamp (again, by definition this can
never be more than \vts{}) in the closure of the collective knowledge bases of
all agents in \vgr{} at time \vts{}.


\section{Properties of \texorpdfstring{\tkbl{}}{TKBL}}
\label{sec:properties-of-tkbl:t}

As mentioned before, while \learns{} is used to represent the time instant in
which someone learns a piece of information, \knows{} is the lasting effect of
learning something. The relationship between the two operators is demonstrated
by the following two properties.

Given an agent \vag{} and a \tkbl{} formula \vf{}, we can formulate the
following as the \emph{learning axiom}:
\[
   \learns_\vaga \vf \then \knows_\vaga \vf \label{la} \tag{\la}
\]
The intuition behind \ref{la} is that in order for the premise to be true in
any trace \( \vtrace \in \traceset \) at time \( \vts \in \tracets \), it has
to be the case that \( (\vf, \vts) \in \tcl(\kb_\vaga^{\vtrace[\vts]}) \). But
then the conclusion is trivially satisfied by the same \( (\vf, \vts) \) being
in \vaga{}'s knowledge base closure.

However, the converse does not hold. It is often the case that \( (\vfb, \vts')
\in \tcl(\kb_\vagb^{\vtrace[\vts]}) \) and \( \vts' < \vts \), in which case
\vfb{} is simply knowledge obtained in the past (\( \vts' \)). Then by
definition \( \vtrace, \vts \sat \knows_\vagb \vfb \) since \vfb{} is in
\vagb{}'s knowledge base closure with any timestamp, but at the same time \(
\vtrace, \vts \; \cancel{\sat} \; \learns_\vagb \vfb \) since \( \vts' < \vts
\).

Moreover, the following property, henceforth called the \emph{perfect recall
axiom}, expresses the relationship between knowledge and time in \tkbl{}:
\[
   \knows_\vaga \vf \then \al \knows_\vaga \vf \label{pr} \tag{\pr}
\]
In other words, if an agent knows something, then they will always know it. As
mentioned before, knowledge here is monotonic -- agents cannot forget anything
they have learned. Once \( (\vf, \vts) \) is in an agent's knowledge base
closure, there is no way to remove it in the future since \tcl{} only adds new
knowledge and we have not formalized any notion of taking away knowledge, or
forgetting, so the same \( (\vf, \vts) \) will still be there at any point in
the future, thus satisfying the consequence of \ref{pr}.

By combining \ref{la} and \ref{pr}, we also have the property that once an
agent learns something, they will always know it:
\[
   \learns_\vaga \vf \then \al \knows_\vaga \vf
\]


\section{Examples}

\begin{figure}
\begin{center}
\begin{tikzpicture}
   \tikzstyle{node}=[circle, draw, minimum size=2em]
   \tikzstyle{label}=[font=\small]
   \tikzstyle{kb}=[font=\footnotesize, fill, fill=gray!10, align=left, inner sep=5pt]
   \node[node] (a) at (0, 2)  {};
   \node[node] (b) at (-3, 0) {};
   \node[node] (c) at (3, 0) {};

   \node at (0, 2.65) {Alice};
   \node at (-3.65, -0.65) {Bob};
   \node at (3.65, -0.65) {Charlie};

   \node[kb] (ka) at (4, 3) {
      \( (\forall \eta. \mword{post}(\vbob, \eta) \then \mword{loc}(\vbob, \eta), 1) \) \\
      \( (\mword{post}(\vbob, 1), 3) \)
   };
   \node[kb] (kb) at (-4.5, 2.4) {
      \( (\forall x. \mword{bYear}(x) \land \mword{bMonth}(x) \land \) \\
      \( \;\;\;\mword{bDay}(x) \then \mword{age}(x), 1) \) \\
      \( (\mword{bMonth}(\valice), 3) \) \\
      \( (\mword{bDay}(\valice), 4) \) \\
      \( (\mword{loc}(\vbob, 1), 7) \)
   };
   \node[kb] (kc) at (6, 0) {
      \( (\mword{bYear}(\valice), 5) \) \\
      \( (\mword{loc}(\vbob, 1), 7) \)
   };

   \path[]
      (a) edge [latex-latex, bend right=10] node [label, sloped, anchor=south] {\emph{friends}} (b)
      (c) edge [-latex, bend right=10, dashed] node [label, sloped, anchor=south] {\emph{friendRequest}} (a)
      (b) edge [-latex, bend right=20] node [label, below] {\emph{blocked}} (c)
      (ka) edge [very thick, gray!10] (a)
      (kb) edge [very thick, gray!10] (b)
      (kc) edge [very thick, gray!10] (c);
\end{tikzpicture}
\caption[Small \tsnm{} with knowledge bases]
        {We revisit the previous Example \ref{ex:tsnm} by making the knowledge
        bases of agents explicit. In the picture, these are depicted as grey
        rectangles connected to the node representing the owner agent. Note
        that we use simple dummy timestamps to simplify the picture
        (originally, timestamp \( \vts = 1 \) would mean January 1, 1970,
        00:00:00.001).}
\label{fig:tsnm-revisited}
\end{center}
\end{figure}

\begin{example}
   At this point we can revisit the previous example (Ex. \ref{ex:tsnm}).
   Figure \ref{fig:tsnm-revisited} contains the same set of agents, connections
   and permissions as before, but now the knowledge bases have been made
   explicit. Each rectangle represents the accumulated knowledge of an agent.

   The knowledge bases of both Bob and Charlie contain the timestamped formula
   \( (\mword{loc}(\vbob, 1), 7) \), which means that they both learned Bob's
   location 1 at time 7. We use the second argument of \mword{loc}, 1, as a
   resource identifier to be able to tie together related information. This is
   used for example in the first formula in Alice's knowledge base, which says
   that she can derive the location of Bob if she can access his post, where
   the \( \mword{loc}(\vbob, \eta) \) and \( \mword{post}(\vbob, \eta) \) have
   the same identifier, meaning that Bob's location is attached to his post in
   some way. Since Alice learned \( \mword{post}(\vbob, 1) \) at time 3, she
   can use this rule to derive the timestamped formula \( (\mword{loc}(\vbob,
   1), 3) \). Note that according to the closure definition (Def.
   \ref{def:timed-closure:t}), the timestamp of the new piece of information is
   the maximum out of the two formulae used to derive it (1 and 3).

   Agents can also combine their knowledge. For instance, if \( G = \{ \vbob,
   \vcharlie \} \), then \( \disknow_G \mword{age}(\valice) \) holds at the
   time of this particular \tsnm{}. Bob knows Alice's day and month of birth,
   Charlie knows the year Alice was born. Moreover, Bob knows that once he
   knows someone's day, month, and year of birth, he can infer the age of the
   person in question. And so, if Bob and Charlie combined their knowledge at
   time 5 (since that is when the last ``piece of the puzzle'' was obtained) or
   later, they would be able to find out Alice's age.
\end{example}

\begin{example}
   To demonstrate how the satisfiability relation works, consider an \osn{}
   with at least two events: \mword{checkIn}, which discloses a user's location
   to all their friends, and \mword{openFeed}, which retrieves all information
   a user has access to, including locations. Let us assume \vtsn{} is the
   \tsnm{} from Fig. \ref{fig:tsnm-revisited} and it represents the \osn{} at
   time 7. Afterwards, it undergoes the following evolution:
   \[
      \vtsn \trans{\{ \mword{checkIn}(\valice) \}, \; 8} \vtsn' \trans{\{
      \mword{openFeed}(\vbob) \}, \; 9} \vtsn''
   \]
   In other words, Alice makes her location public to her friends at time 8 and
   then Bob opens his news feed at time 9. Using the more commonly used
   notation, we can write
   \[
      \vtrace = \tuple{(\vtsn, 7), (\vtsn', 8), (\vtsn'', 9)}.
   \]
   We can use the satisfiability relation (Def.
   \ref{def:satisfiability-relation:t}) to determine whether Bob learns Alice's
   location after she discloses it to her friends. More precisely, we are
   interested in whether it is the case that
   \begin{align*}
      \vtrace, 7 \sat \al(\mword{friends}(Alice, Bob) \land
      \mword{checkIn}(\valice, 7) \then \exists x. \ev \learns_\vbob
      \mword{loc}(\valice, x)).
   \end{align*}
   According to the definition, in order for \( \al \vf \) to hold, \vf{} has
   to hold in all \tsnm{}s that are not older than the guiding timestamp on the
   left-hand side, which in our case is 7. Therefore, we must in fact check
   every \tsnm{} in \vtrace{}.

   As for the premise, the predicate \( \mword{friends}(\valice, \vbob) \) was
   true in \vtsn{} and since no \emph{unfriend} event took place, it is the
   case that \( (\valice, \vbob) \in A_\mword{friends}^{\vtrace[\vts]} \) for
   all \( \vts \geq 7 \), that is, Alice and Bob are friends in all three
   \tsnm{}s in \vtrace{} -- at time 7, 8 and 9. The predicate \(
   \mword{checkIn}(\valice, 7) \) in this case represents that Alice executed
   the \( \mword{checkIn} \) event after time 7. Since according to the
   transition relation the \( \mword{checkIn} \) event was executed at time 8,
   the associated predicate is true at time 8 and 9: \(
   \mword{checkIn}(\valice, 7) \in \kb_\environ^{\vtrace[\vts]} \) for \( \vts
   \geq 8 \).

   Moving over to the right-hand side of the implication, \( \ev{} \vfb \)
   requires there to be at least one \tsnm{} not older than the guiding
   timestamp such that \vfb{} holds. In our case, there has to be some \tsnm{}
   in which a timestamped \( \mword{loc}(\valice, x) \) for some \( x \) is in
   \( \tcl(\kb_\vbob^{\vtrace[\vts]}) \) for some \( \vts \) satisfying the
   above condition. In this case Bob does not need to infer any knowledge: once
   he opens his news feed at time 9, he will see Alice's location, so \(
   (\mword{loc}(Alice, 1), 9) \) (where 1 is the resource identifier of the
   location) will appear in \( \kb_\vbob^{\vtrace[9]} \).

   To conclude, since the premise holds at time 8 and 9 and in both cases, Bob
   eventually learns Alice's location at 9, the property holds.
\end{example}
