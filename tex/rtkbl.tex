While \tkbl{} utilized the temporal operators \al{} and \ev{} to capture the
dynamic character of the framework, \rtkbl{} promotes timestamps themselves to
a syntactic component of the language. Since both \al{} and \ev{} are derivable
in \rtkbl{}, these operators are not present in the basic syntax of \rtkbl{}.

This transition from temporal operators to timestamps being part of the syntax
is the main difference between the temporal \tkbl{} and the real-time \rtkbl{}.


\section{Syntax of \texorpdfstring{\rtkbl{}}{RTKBL}}
\label{sec:syntax-of-rtkbl:rt}

We reuse the standard definition of terms from before (Def. \ref{def:term:t})
to build the syntax of the new logic, where each of the basic building blocks
contains a timestamp.

\begin{definition}[Syntax of \rtkbl{}]
\label{def:syntax-of-rtkbl:rt}
   Given
   agents \( \vaga, \vagb \in \ag \),
   a nonempty set of agents \( \vgr \subseteq \ag \),
   a timestamp \( \vts \),
   an event \( \vevt \in \evt \),
   a variable \( x \),
   predicate symbols \( a_n(\vaga, \vagb, \vts), c_m(\vaga, \vagb, \vts),
   p(\vterm, \vts)\) where \( m \in \conns \) and \( n \in \perms \),
   the \emph{syntax of the \textrtkbl{} \rtkbl{}} is inductively defined as:
   \begin{align*}
      \phi &::= \rho \mid
                \phi \land \phi \mid
                \neg \phi \mid
                \forall x. \phi \mid
                \knows_\vaga^\vts \phi \mid
                \comknow_\vgr^\vts \phi \mid
                \disknow_\vgr^\vts \phi
                \\
      \rho &::= c_m(\vaga, \vagb, \vts) \mid
                a_n(\vaga, \vagb, \vts) \mid
                \vpred(\vterm, \vts) \mid
                \occur(\vevt, \vts)
   \end{align*}   
\end{definition}

We will use \frtkbl{} to denote the set of all well-formed \rtkbl{} formulae.

As the definition suggests, apart from the now absent temporal operators
mentioned earlier, the syntax introduces a number of new notions.

\paragraph{Timestamped Predicates}
Timestamps are explicitly part of each predicate, including connections and
actions. The meaning of the timestamp attached to a predicate is that it
should capture the moment where that particular predicate was true. In this
sense, timestamped predicates can be seen as functions of time. For instance,
if Alice and Bob were friends in a certain time period, then the predicate \(
\mword{friends}(\valice, \vbob, \vts) \) should be true for all \vts{} falling
into the period, and false for all \vts{} outside.

\paragraph{Timestamped Knowledge Modalities}
Timestamps are now also part of the knowledge modalities \( \knows, \comknow,
\disknow \). These timestamps represent the moment in time when a particular
piece of knowledge was obtained. For example, the meaning of the formula \(
\knows_\valice^\vts \mword{friends}(\vbob, \vcharlie, \vts') \) is that Alice
learns at time \vts{} that Charlie was friends with Bob at time \( \vts' \).

It should also be noted that \rtkbl{} does not include separate modalities for
learning and knowledge, as was the case in \tkbl{}. Semantically, here the
\knows{} modality stands for learning: it is true if the agent in question
acquires a certain piece of information at the moment given by the timestamp
(Def. \ref{def:satisfiability-relation:rt}). Knowledge in the sense of the
\knows{} of \tfppf{}, i.e., simply having a piece of information in the
knowledge base closure, can be derived from the \knows{} modality of \rtfppf{}
thanks to the ability to quantify over timestamps -- we can use this to
translate the meta language quantification in Def.
\ref{def:satisfiability-relation:t} to quantification on the syntactic level of
\rtkbl{}. Semantically, evaluating \( \knows_\vag \vf \) (for the \tkbl{}
\knows{}) at time \vts{} in a trace has the same interpretation as the \rtkbl{}
formula \( \exists \vts'. \vts' \leq \vts \land \knows_\vag^{\vts'} \vf \),
which can be read as ``there is a time in the past when \vag{} learned \vf{}''.

Using timestamps in both the knowledge modalities and predicates enables us to
make a crucial distinction impossible in \tfppf{}: we can now separate and
capture the time of a predicate being valid in the \osn{} and the time when an
agent learns it. This allows for more fine-grained privacy policies (Chapter
\ref{chapter:rtppl}).

\paragraph{Special Event Predicate}
Now that events are explicitly part of any \rtfppf{} trace, the \(
\occur(\vevt, \vts) \) predicate has been introduced to be able to
syntactically capture the moment when a specific event occurred. This makes it
possible to reason about time relative to the time of the event, enabling the
user to define policies such as ``if someone unfriends me, they are not allowed
to send me a friend request''. Note that now that events are explicit in the
trace, they need not be included in the \kb{} of the environment.

\subsection{Notation}
\label{subsec:rtkbl-notation:rt}

Given agents \( \vaga, \vagb \in \ag \), a nonempty group \( \vgr \subseteq \ag
\), and an action \( a_n \), we define, as before in \tfppf{} (Sec.
\ref{subsec:rtkbl-notation:rt}):

\begin{center}
\begin{tabular}{rcl}
   \( P^{\vagb}_\vaga a_n \) & \( := \) & \( a_n(\vaga, \vagb) \) \\
   \( SP^{\vagb}_\vgr a_n \) & \( := \) & \( \bigvee_{\vaga \in \vgr}
   a_n(\vaga, \vagb) \) \\
   \( GP^{\vagb}_\vgr a_n \) & \( := \) & \( \bigwedge_{\vaga \in \vgr}
   a_n(\vaga, \vagb) \)
\end{tabular}
\end{center}

Again, these are read: ``\vaga{} is permitted to execute \( a_n \) to
\vagb{}'', ``someone in \vgr{} is permitted to execute \( a_n \) to \vagb{}'',
and ``everyone in \vgr{} is permitted to execute \( a_n \) to \vagb{}''. 

Furthermore, we also capture the statements ``someone in group \vgr{} knows
\vf{}'' and ``everyone in group \vgr{} knows \vf{}'' by the following shortcut
modalities:

\begin{center}
\begin{tabular}{ll}
   \( \someone_\vgr \vf \triangleq \bigvee_{\vag \in \vgr} \knows_\vag \vf \)
   &
   \( \everyone_\vgr \vf \triangleq \bigwedge_{\vag \in \vgr} \knows_\vag \vf
   \) \\
\end{tabular}
\end{center}

Note that semantically, these will be the \rtkbl{} equivalents of \tkbl{}'s
\someone\learns{} (``someone learned'') and \everyone\learns{} (``everyone
learned'') modalities.


\section{Semantics of \texorpdfstring{\rtkbl{}}{RTKBL}}
\label{sec:semantics-of-rtkbl:rt}

In order to define a \rtkbl{}-based inference engine for agents, the definition
of timed derivation (Def. \ref{def:timed-derivation:t}) can be reused entirely,
just replacing \tcl{} with the real-time version we are about to define,
\rtcl{}. We will also reuse most of the \tcl{} definition (Def.
\ref{def:timed-closure:t}), since the way it captures learning and inferring
new knowledge applies here as well. We have to, however, update it with
timestamps where missing.  To make the following definition more readable, we
use the placeholder variable name \plc{} when the variable in question is not
used again in the same context. If there are more instances of \plc{} in the
same formula, they refer to different variables.

\begin{definition}[\textccrtcl{}]
\label{def:real-time-closure:t}
   Given the knowledge base of an agent \vaga, \( \kb_\vaga \), the
   \emph{real-time closure of \( \kb_\vaga \), \rtclkba{}}, satisfies the
   following properties:
   \begin{enumerate}
      \item
         Properties 1, 2, 3 in Def. \ref{def:timed-closure:t}, with \rtcl{}
         instead of \tcl{}.
      \item
         If \( (\vfa, \vts) \in \rtclkba \) then \( (\knows_\vaga^\vts \vfa,
         \vts) \in \rtclkba \).
      \item
         If \vfa{} is provable in the axiomatization \sfiveaxioma{} from
         \rtclkba{} then \( \vfa \in \rtclkba \). Formally:
         \begin{itemize}
            \item[A1, R2 -]
               As A1, R2 in Def. \ref{def:timed-closure:t}, with \rtcl{}
               instead of \tcl{}.
            \item[A2 -]
               If \( (\knows_\vag^{\plc} \vfa, \vts) \in \rtclkba \) and \(
               (\knows_\vaga^{\plc} (\vfa \implies \vfb), \vts') \in \rtclkba
               \), then \( (\knows_\vaga^{\max(\vts, \vts')} \vfb, \max(\vts,
               \vts')) \in \rtclkba \).
            \item[A3 -]
               If \( (\knows_\vaga^\plc \vfa, \vts) \in \rtclkba \), then \(
               (\vfa, \vts) \in \rtclkba \).
            \item[A4 -]
               If \( (\knows_\vaga^{\vts'} \vf, \vts) \in \rtclkba \), then \(
               (\knows_\vaga^{\vts} \knows_\vaga^{\vts'} \vf, \vts) \in
               \rtclkba \).
            \item[A5 -]
               If \( (\vf, \vts) \notin \rtclkba \), then \( (\neg
               \knows_\vag^\vts \vf, \vts) \in \rtclkba \).
            \item[R2 -]
               If \( (\vf, \vts) \) is provable from no assumptions (i.e.,
               \vf{} is a tautology), then \( (\knows_\vag^\vts \vf, \vts) \in
               \rtclkba \).
            \item[C1 -]
               \( (\everyone_\vgr^{\vts'} \vf, \vts) \in \rtclkba \) iff \(
               (\bigwedge_{\viter \in \vgr} \knows_\viter^{\vts'} \vf, \vts)
               \in \rtclkba \).
            \item[C2 -]
               \( (\comknow_\vgr^{\vts'} \vf, \vts) \in \rtclkba \) iff \(
               (\everyone_\vgr^{\vts'} (\vf \wedge \comknow_\vgr^{\vts'} \vf),
               \vts) \in \rtclkba \).
            \item[RC1 -]
               If \( (\vfa \implies \everyone_\vgr^{\vts'} (\vfb \wedge \vfa),
               \vts) \) is provable from no assumptions, then \( (\vfa \implies
               \comknow_\vgr^{\vts'} \vfb, \vts) \in \rtclkba \).
            \item[D1 -]
               \( (\disknow_{\{\vaga\}}^{\vts'} \vfa, \vts) \in \rtclkba \) iff
               \( (\knows_\vaga^{\vts'} \vfa, \vts) \in \rtclkba \).
            \item[D2 -]
               If \( (\disknow_\vgr^{\vts'} \vfa, \vts) \in \rtclkba \), then
               \( (\disknow_{\vgr'}^{\vts'} \vfa, \vts) \in \rtclkba \) if \(
               \vgr \subseteq \vgr' \).
            \item[DA2-DA5]
               Properties A2, A3, A4 and A5, replacing the modality \(
               \knows_\vag^\vts \) with the modality \( \disknow_\vgr^\vts \)
               for each axiom.
         \end{itemize}
   \end{enumerate}
\end{definition}

The basic principle behind the rules in the previous definition is the same as
before. As was the case of \tkbl{}, the semantics of \rtkbl{} is also given in
terms of a satisfiability relation.

\begin{definition}[Satisfiability Relation]
\label{def:satisfiability-relation:rt}
   Given
   a well-formed trace \( \vtrace \in \traceset \),
   agents \( \vaga, \vagb \in \ag \),
   a finite set of agents \( \vgr \subseteq \ag \),
   formulae \( \vfa, \vfb \in \frtkbl \),
   \( m \in \conns \),
   \( n \in \perms \),
   \( o \in \dom \),
   a variable \( x \),
   an event \( \vevt \in \evt \),
   and a timestamp \vts{},
   the \emph{satisfiability relation \( \sat{} \subseteq \traceset{} \times
   \frtkbl{}\)} is defined as shown in Fig.
   \ref{fig:satisfiability-relation:rt}.
\end{definition}

\begin{figure}
\begin{center}
  \begin{tabular}{lcl}
    \( \vtrace, \vts \sat \occur(\vevt, \vts') \) &
    iff &
    \( \vevt \in \vevts \) where \( (\vrtsn, \vevts, \vts') \in \vtrace \) \\
    \\
    \( \vtrace, \vts \sat \neg \vfa \) &
    iff &
    \( \vtrace, \vts \; \cancel{\sat} \; \vfa \) \\
    \( \vtrace, \vts \sat \vfa \land \vfb \) &
    iff &
    \( \vtrace, \vts \sat \vfa \) and \( \vtrace, \vts \sat \vfb \) \\
    \( \vtrace, \vts \sat \forall x. \vfa \) &
    iff &
    for all \( v \in D_o^{\vtrace[\vts]} \), \( \vtrace, \vts \sat \vfa[v / x]
    \) \\
    \\
    \( \vtrace, \vts \sat c_m(\vaga, \vagb, \vts') \) &
    iff &
    \( (\vaga, \vagb) \in C_m^{\vtrace[\vts']} \) \\
    \( \vtrace, \vts \sat a_n(\vaga, \vagb, \vts') \) &
    iff &
    \( (\vaga, \vagb) \in A_n^{\vtrace[\vts']} \) \\
    \\
    \( \vtrace, \vts \sat \vpred(\vterm, \vts') \) &
    iff &
    \( (\vpred(\vterm, \vts'), \vts') \in \kb^{\vtrace[\vts']}_\environ \) \\
    \\
    \( \vtrace, \vts \sat \knows^{\vts'}_\vaga \vfa \) &
    iff &
    \( (\vfa, \vts') \in \rtcl(\kb^{\vtrace[\vts']}_\vag) \) \\
    \\
    \( \vtrace, \vts \sat \comknow^{\vts'}_\vgr \vfa \) &
    iff &
    \( \vtrace, \vts' \sat \everyone_\vgr^{\vts', \vnat} \vfa \) for \( \vnat =
    1, 2, \ldots \) \\
    \( \vtrace, \vts \sat \disknow^{\vts'}_\vgr \vfa \) &
    iff &
    \( (\vfa, \vts') \in \rtcl(\bigcup_{\viter \in
    \vgr}\kb^{\vtrace[\vts']}_\viter) \)
  \end{tabular}
\end{center}
\caption[Satisfiability relation for \rtkbl{}]%
        {The semantics for \rtkbl{} is given in terms of the satisfiability 
         relation.}
\label{fig:satisfiability-relation:rt}
\end{figure}

One of the differences from the previous satisfiability relation (Def.
\ref{def:satisfiability-relation:t}) is the meaning of the timestamp on the
left-hand side. In \tkbl{}, this timestamp marked the \tsnm{} in the trace in
which the right-hand side formula was meant to be checked.  Here, we use the
timestamps in the syntax to guide the checking process to the corresponding
\rtsnm{}s. When checking connections and actions at time \vts{}, we check
whether the corresponding relation of the \rtsnm{} at time \vts{} contains the
agents in question. Checking a predicate with timestamp \vts{} is equivalent to
looking into the knowledge base of the environment at time \vts{} and looking
for the predicate. Determining whether an agent \vaga{} knows \vf{} at time
\vts{} translates to looking into the closure of \vaga{}'s knowledge base at
time \vts{} to see whether \vaga{} learns \vf{} at time \vts{}.  The
left-hand-side timestamp is only used in the quantification case to determine
the \rtsnm{} in the trace whose relational structure should be used to obtain
the values to substitute for the quantified variable.

The semantics of the new special event predicate \( \occur(\vevt, \vts) \)
boils down to looking into the set of events that happened in the trace at time
\vts{} and determining whether \vevt{} is one of them.


\section{Examples}
\label{sec:rtkbl-examples}

\begin{figure}
\begin{center}
\begin{tikzpicture}
   \tikzstyle{node}=[circle, draw, minimum size=2em]
   \tikzstyle{label}=[font=\small]
   \tikzstyle{kb}=[font=\footnotesize, fill, fill=gray!10, align=left, inner sep=5pt]
   \node[node] (a) at (0, 1.5)  {};
   \node[node] (b) at (-2, 0) {};
   \node[node] (c) at (2, 0) {};

   \node at (0, 2.25) {Alice};
   \node at (-2.8, -0.65) {Bob};
   \node at (2.8, -0.65) {Charlie};

   \node[kb] (ka) at (4, 3) {\( (\mword{event}(1, 2), 1) \)};
   \node[kb] (kb) at (-4.5, 2.4) {
      \( (\forall x. \forall y. \forall z. (\mword{picture}(x, y, z) \then \) \\
      \( \;\;\; \mword{location}(x, y, z)), 1) \)
   };
   \node[kb] (kc) at (6, 1) {
      \( (\forall x. \forall y. \forall z. (\mword{location}(x, y, z) \land \) \\
      \( \;\;\; \mword{event}(y, z) \then \) \\
      \( \;\;\; \mword{attending}(x, y, z)), 1) \)
   };

   \path[]
      (a) edge [latex-latex, bend right=10] node [label, sloped, anchor=south] {\emph{friends}} (b)
      (b) edge [latex-latex, bend right=10] node [label, sloped, anchor=south] {\emph{friends}} (c)
      (c) edge [-latex, bend right=10] node [label, sloped, anchor=south] {\emph{follower}} (a)
      (ka) edge [very thick, gray!10] (a)
      (kb) edge [very thick, gray!10] (b)
      (kc) edge [very thick, gray!10] (c);
\end{tikzpicture}
\caption[\rtsnm{} with three agents]
        {A \rtsnm{} with three agents, the connections between them, and their
        knowledge bases. Here, Bob is able to derive the location of a picture
        if they share the same resource identifier, and Charlie is able to
        conclude that if the time and place of someone's location correspond to
        that of a known event, the person in question is attending the event.
        Meanwhile, Alice found out about an event with identifier 1 which
        happens at time 2.}
\label{fig:rtsnm}
\end{center}
\end{figure}

\begin{example}
\label{ex:rtkbl}
   Consider Fig. \ref{fig:rtsnm} which shows a \rtsnm{} with Alice, Bob and
   Charlie, the connections between them, and their knowledge bases. In this
   case, the predicates used have the following meaning; we use \(
   \mword{picture}(x, y, z) \) for ``there is a picture posted by \( x \) with
   resource identifier \( y \)'', and similarly for \mword{location}; \(
   \mword{event(x, y)} \) stands for ``there is an event with resource
   identifier \( x \)'', and \( \mword{attending}(x, y, z) \) is ``agent \( x
   \) is attending event with identifier \( y \)''. In all cases, the remaining
   argument is the timestamp representing when that particular piece of
   information was true.

   Let \( \vrtsn_0 \) be the \rtsnm{} modeled in the picture and let \vtrace{}
   be the well-formed trace
   \begin{align*}
      \vtrace = \tuple{
         &(\vrtsn_0, \vevts_0, 1), \\
         &(\vrtsn_1, \{ \mword{postPicture}(\valice) \}, 2), \\
         &(\vrtsn_2, \{ \mword{openFeed}(\vcharlie) \}, 3), \\
         &(\vrtsn_3, \{ \mword{openFeed}(\vbob) \}, 4)
      },
   \end{align*}
   where the \( \mword{postPicture}(\valice) \) event -- which transforms \(
   \vrtsn_0 \) to \( \vrtsn_1 \) at time 2 -- represents Alice making a picture
   public to her friends and followers and the meaning of the \mword{openFeed}
   event is the same as before: it refers to a user accessing all information
   available to them in the \osn{}.

   The knowledge evolution from \( \rtsn_0 \) to \( \rtsn_3 \) is as follows.
   At time 2, a picture with resource identifier 1 (that is, tied to event 1)
   taken by Alice appears in the network. Charlie then learns about the picture
   at time 3, while Bob learns about it at time 4. In addition, let us assume
   both Bob and Charlie learn about event 1 when they open their news feed.

   Let us now check the following properties on \vtrace{}:
   \[
      \vtrace, 3 \sat \neg\knows_\vbob^3 \mword{location}(Alice, 1, 2)
      \label{negknow} \tag{1}
   \]
   \[
      \vtrace, 4 \sat \knows_\vbob^4 \mword{location}(Alice, 1, 2)
      \label{know} \tag{2}
   \]
   To check whether Bob does not learn Alice's location 1 from time 2 at time
   3, we consult the satisfiability definition (Def.
   \ref{def:satisfiability-relation:rt}). We look into the closure of Bob's
   knowledge base at time \( \vtrace[3] = \vrtsn_2 \). Since Bob could not
   learn or infer Alice's location from time 2 at time 3, \(
   (\mword{location}(\valice, 1, 2), 3) \notin \rtcl(\kb_\vbob^{\vtrace[3]})
   \), and so property (\ref{negknow}) holds.

   However, as soon as Bob opens his news feed at time 4, he learns about
   Alice's picture, so \( (\mword{picture}(\valice, 1, 2), 4) \in
   \rtcl(\kb_\vbob^{\vtrace[4]}) \). He is then able to use the rule in his
   knowledge base to learn \( \mword{location}(\valice, 1, 2), 4) \), which
   will be in the closure of his knowledge base at time 4, thus fullfiling
   (\ref{know}).
\end{example}

\begin{example}
\label{ex:rtkbl-2}
   Using everything established in Example \ref{ex:rtkbl}, let us also check a
   property regarding the knowledge of multiple agents at once. Let \( \vgr =
   \{ \vbob, \vcharlie \} \).
   \[
      \vtrace, 4 \sat \disknow_\vgr^4 \mword{attending}(\valice, 1, 2)
      \label{dis} \tag{1}
   \]
   To answer whether Bob and Charlie together are able to learn at time 4 about
   Alice attending event 1 at time 2, let us consider what they know
   independently at 4. According to the previous example, \(
   (\mword{location}(\valice, 1, 2), 4) \in \rtcl(\kb_\vbob^{\vtrace[4]}) \).
   Charlie learns about Alice's picture 1 from time 2 as well as about event 1
   from time 2 once he opens his news feed at time 3. He is, however, unable to
   use the rule he has had in his \kb{} from time 1 to infer anything new from
   the two pieces of information.

   The collective knowledge of Bob and Charlie at time 4 contains all of this.
   And so, since both \( (\mword{location}(\valice, 1, 2), 4) \) and \(
   (\mword{event}(1, 2), 3) \) are in \( \rtcl(\bigcup_{\viter \in \vgr}
   \kb_\viter^{\vtrace[4]}) \), the old rule of Charlie's can be used so that
   we get
   \[
      (\mword{attending}(\valice, 1, 2), 4) \in \rtcl\left(\bigcup_{\viter \in
      \vgr} \kb_\viter^{\vtrace[4]}\right),
   \]
   which is the interpretation of (\ref{dis}).
\end{example}
