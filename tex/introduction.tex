Online social networks (\osn{}s) such as Facebook and Twitter have become an
important part of people's lives worldwide. As much as 76\% of all internet
users in the United States use at least one \osn{} nowadays, which corresponds
to a nearly tenfold increase from 2005 \cite{usage-usa}. Another survey from
2015, targeting 32 different countries across the globe, ranging from Kenya, to
Russia, Brazil, Poland, India, or Thailand, reports that the median of \osn{}
users amounts to a similar number: 82\% of adult internet users
\cite{usage-developing}.\footnote{To be more specific about what we mean when
we say \osn{}s: In the aforementioned surveys, when the respondents were asked
about their habits, the researchers most often gave Facebook and Twitter as the
prime examples of \osn{}s. In countries where local sites were also prominent,
these were mentioned as well (for example, VK in Russia and Renren in China).

More generally, we can adopt the definition in \cite{sn-def} by identifying
three distinguishing characteristics of social network sites. These require
that users on the site: (a) have uniquely identifiable profiles consisting of
data provided by themselves, other users, and/or system, (b) can publicly
articulate connections that can be viewed and traversed by others, and (c) are
able to consume, produce and/or interact with streams of user-generated
content.
}

Aside from helping make socializing number one internet activity
\cite{usage-developing}, this popularity has also given rise to new concerns,
the issue of preserving the privacy of social network users being one of the
most fundamental. Even though the right to privacy is recognized as one of the
basic human rights \cite{rights}, \osn{} users are not at all confident about
their data staying private and secure. In a recent series of interviews
\cite{attitudes}, only 11\% of Americans believed their data was safe with
social media sites, compared to 69\% of respondents who were ``not too
confident'' or ``not at all confident''. On the other hand, 93\% of adults in
the same study felt it was important to be able to control who was able to
access information about them.

Although users provide \osn{}s with a great amount of personal data, the tools
to govern one's own information offered by popular \osn{}s are limited, and
sometimes even options one can set turn out to be different from the user's
expectations. A study from 2011 \cite{facebook-privacy}, which focused on
Facebook, found that privacy settings match the expectations of the users only
37\% of the time, and when an instance of this disparity between expectations
and reality occurs, the resulting difference is almost always undesirable: more
content is exposed than expected, not the other way around.

There are, however, ways to address the issue. One suggestion is to assist
users in managing their privacy, for instance by making \osn{}s such as
Facebook more sensitive to social groups by grouping users into communities
\cite{facebook-privacy}.

A more general solution is to target privacy policies themselves and their
expressive power and clarity. The authors of the formal privacy policy
framework \fppf{} (\cite{ppf-14, ppf}) see the option of richer and more
fine-grained privacy policies as a potential solution. In their framework, the
user is given the opportunity to define their privacy policy using logic, and
the behavior of the social network with respect to such policy can only be
classified as being in compliance with it or not; there is no middle ground, no
uncertainty where the user has to hope their privacy settings will work like
they expect. Able to capture and work with virtually any social network
structure, the framework can be used as an alternative to current privacy
policies. This can be demonstrated by a working prototype implementing some of
the privacy policies in an open-source social network, with full integration
underway.

Writing privacy policies based on \fppf{}, as opposed to the few hard-coded
options currently being provided by popular \osn{}s, is a huge improvement.
However, there are cases, interesting from the user's perspective, where the
capabilities of \fppf{} fall short. One of these areas is the possibility of
writing privacy policies sensitive to real time. For instance, someone might
want to prevent her boss from knowing her whereabouts outside office hours. Or
someone else might be interested in hiding any photos of him taken on New
Year's Eve. Whatever their reason is, we firmly believe that the users should
have as much control over their private data as possible. Allowing for even
more fine-grained policies on top of \fppf{}, by increasing their sensitivity
to real time aspects, is a step closer to this goal.


\section{Thesis Overview}

Apart from the introduction (Chap. \ref{chapter:introduction}), the
preliminaries and literature review (Chap. \ref{chapter:preliminaries}), the
discussion (Chap. \ref{chapter:discussion}), and the conclusion (Chap.
\ref{chapter:conclusion}), the thesis is organized into two major parts.

Two standalone time-sensitive extensions of \fppf{} are introduced, each in a
separate part. Since the high-level structure of both frameworks is very
similar, each part is structured in the same way. There are three chapters,
each describing the three major components of each framework -- the underlying
\osn{} model, the knowledge-based logic used to reason about agents and the
information they possess, and the privacy policy language, built atop the
knowledge-based logic.

The first framework we propose, the \texttfppf{} (\tfppf{}), uses a privacy
policy language enhanced with time fields, which make it possible to define a
possibly recurring real-time window in which a policy should be enforced. The
knowledge-based logic of \tfppf{} utilizes the standard box and diamond
operators found in various temporal logics \cite{logic-cs} to be able to reason
about time. \tfppf{} is introduced in Part \ref{part:tfppf}.

The second framework proposed in this thesis, the \textrtfppf{} (\rtfppf{}),
represents an alternative way of reasoning about time in \osn{}s by
incorporating timestamps representing a particular millisecond right into the
syntax of the knowledge-based logic. Privacy policies written using the privacy
policy language powered by the logic allow for even more fine-grained and
flexible policies which need not be constrained by a time window, but can, for
example, react to events happening in the \osn{}. \rtfppf{} is described in
Part \ref{part:rtfppf}.


\section{Scope}

Our aim is to develop a time-sensitive formal framework. To this end we define
a number of formalisms, summarized in the next section. Compared to the
previous \fppf{}, we do not formalize the notion of \osn{} instantiation, nor
do we introduce any concrete operational semantics that transform one \osn{}
model to another. Formalization of what it means for a framework to be privacy
preserving is also out of scope of this thesis. Moreover, it is not our primary
aim to explore the theoretical properties of the formalisms in greater detail
in this thesis; we mainly aim to utilize them.


\section{Contributions}

The contributions presented in this thesis can be summarized as follows.

\begin{itemize}
   \item
      Building on the existing framework \fppf{}, we define two standalone
      temporal frameworks for \osn{}s: \tfppf{} in Part \ref{part:tfppf} and
      \rtfppf{} in Part \ref{part:rtfppf}.
   \item
      A social graph-based \osn{} model is introduced for both frameworks to be
      able to capture an \osn{} at a specific point in time, together with the
      knowledge and relationships between its users. Additionally, we introduce
      the notion of \osn{} evolution for both frameworks using traces, that is,
      sequences of \osn{} models. The definitions and the properties of the
      models can be found in Chapter \ref{chapter:tsnm} for \tfppf{} and in
      Chapter \ref{chapter:rtsnm} for \rtfppf{}.
   \item
      A temporal knowledge-based logic is defined for both frameworks. Together
      with their associated semantics, these logics are used to reason about
      knowledge in \osn{}s in a time-sensitive context. We formally define and
      describe these logics in Chapters \ref{chapter:tkbl} for \tfppf{} and
      \ref{chapter:rtkbl} for \rtfppf{}.
   \item
      We define two privacy policy languages, one for each framework, that
      enable agents in the \osn{} to define their own privacy policies using a
      number of generic templates. A conformance relation is defined for each
      of the languages to determine whether a particular policy is not violated
      in a specific evolution of the \osn{}. Chapters \ref{chapter:tppl} (for
      \tfppf{}) and \ref{chapter:rtppl} (for \rtfppf{}) are devoted to these
      two languages.
\end{itemize}
