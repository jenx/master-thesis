Online social networks (\osn{}s) are an important part of people's lives
worldwide and their users supply them with a large amount of personal data. The
audience of this information is usually restricted by privacy policies which
the users can define. However, these settings are often limited and their
meaning is not clearly defined, leading to discrepancies between the users'
expectations and actual flow of information in the \osn{}.

In this thesis we explored and formalized two concepts of real-time \osn{}
frameworks built upon the formal privacy policy framework \fppf{} \cite{ppf,
ppf-14}. The aim was to develop a time-sensitive formalization of \osn{}s and
the ways information spreads there, ultimately leading to a fine-grained
privacy policy language based on a knowledge-based logic.

The first framework we propose, \tfppf{}, comprises three main components.
First, there is the \texttsnm{} \tsnm{}, which captures the structure of an
\osn{} at a certain point in time, along with its users and the relationships
and permissions between them. Next, we introduce the \texttkbl{} \tkbl{}, which
is used to reason about knowledge in evolving \osn{}s. A satisfiability
relation is then used to determine whether a \tkbl{} formula holds in an
evolving \osn{}. Finally, we define the \texttppl{} \tppl{}, a formal language
to write privacy policies based on \tkbl{}, and we also formalize how to
determine if an \osn{} violates a specific policy. \tppl{} allows the users to
have their policies evaluated either from a point in time forward, or in a
(number of) precisely defined time window(s).

The second framework proposed in this work, \rtfppf{}, also comprises three
parts. First, there is the \textrtsnm{} \rtsnm{}, whose structure is very
similar to that of \tsnm{}. The first major difference comes at the level of
the knowledge-based logic. \rtfppf{}'s logic is called \textrtkbl{} (\rtkbl{})
and contains timestamps on the level of syntax, allowing for a wide range of
time-sensitive formulae. A satisfiability relation is provided as well, to
determine whether a \rtkbl{} formula holds in the dynamic \osn{}. Finally, we
define the \textrtppl{} \rtppl{}, built on \rtkbl{}, together with a
conformance relation.

Each of the frameworks allows users of \osn{}s to define fine-grained,
time-sensitive privacy policies based on formal logic, thus addressing the
problem of privacy policy ambiguity in \osn{}s in a dynamic context. Moreover,
both \tkbl{} and \rtkbl{} can be used directly to reason about knowledge
spreading and evolving in an \osn{}.

Both \tfppf{} and \rtfppf{} constitute a step forward in reasoning about
knowledge and restricting the audience of information in evolving \osn{}s.
