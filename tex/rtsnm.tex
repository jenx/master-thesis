\section[\textccrtfppf{} \rtfppf{}]{\textccrtfppf{} \\ \rtfppf{}}
\label{sec:rtfppf}

The second part of the thesis is devoted to the \textrtfppf{} \rtfppf{}. This
framework aims to solve the delimitations of \tfppf{}, using it as an
intermediate step towards a more fine-grained time-sensitive epistemic logic
and privacy policy language.

Some of the delimitations of \tfppf{} were described in the previous chapters.
Essentially, they can be summed up as follows:
\begin{itemize}
   \item
      Inflexibility. The policies of \tppl{} are only enforced in a fixed time
      window, though that may not be the desired behavior, as illustrated in
      Sec. \ref{subsec:checking-outside-policy-windows:t} and
      \ref{sec:example-privacy-policies:t}.
   \item
      Inability to determine the time when a piece of information was
      up-to-date, or more precisely, when it appeared in the \osn{}. The
      timestamps in the knowledge bases of agents only represent the point of
      learning. If an agent learns some piece of information from a year ago at
      the same time as a piece of information that has just been posted, both
      will be in their knowledge base with a recent timestamp.
   \item
      Limited ability to reason about time relative to what happens in the
      \osn{}. One cannot write policies that react to events in the \osn{}, nor
      is it possible to tie predicates together based on when they were or were
      not true.
\end{itemize}

The logic proposed in this part, \rtkbl{} (Chap.  \ref{chapter:rtkbl}), marks
the transition from the temporal \tkbl{} to a logic which contains timestamps
as a syntactic component. In \rtkbl{}, timestamps become part of every
predicate and knowledge modality, making it possible to distinguish between the
point when a piece of information appeared in the \osn{} and the point when an
agent learns it. To progress from the implicit notion of events in \tfppf{},
they become an explicit part of the \osn{} trace in \rtfppf{}. These and other
differences will be described in the following chapters.

Formally, we define:

\begin{definition}[\textccrtfppf{}]
\label{def:rtfppf:rt}
   The tuple \( \rtfppf{} = \tuple{\rtsn, \rtkbl, \sat, \rtppl, \con} \) is a
   \emph{\textrtfppf{}} where
   \begin{itemize}
      \item
         \rtsn{} is the set of all possible \textplrtsnm{};
      \item
         \rtkbl{} is a \textrtkbl{};
      \item
         \sat{} is a satisfiability relation defined for \rtkbl{};
      \item
         \rtppl{} is a formal \textrtppl{};
      \item
         \con{} is a conformance relation defined for \rtppl{}.
   \end{itemize}
\end{definition}

The structure of \rtfppf{} is very similar to that of \tfppf{}. In fact, some
of the notions defined in \tfppf{}, such as timestamps, can and will be
reused. To reflect this continuity, the rest of this part of the thesis is
organized in the same way as the previous one. There are three chapters, each
describing one of the major components (the underlying model in the rest of
Chapter \ref{chapter:rtsnm}, the logic to reason about knowledge in Chapter
\ref{chapter:rtkbl}, and the privacy policy language in Chapter
\ref{chapter:rtppl}).

Some of the notions in this part will reuse the names from Part
\ref{part:tfppf}, even in cases where the definition might be different. This
is to avoid heavy notation, which could be the case if we decided to use
subscripts and superscripts to distinguish between names used in both
frameworks. In cases when we want to use a previously established name to refer
to the notion from the previous framework, it will be explicitly stated.


\section{\textccrtsnm{} \rtsnm{}}
\label{sec:rtsnm}

The shape of the model used to capture a specific \osn{} in a given moment in
\rtfppf{} is the same as in \tfppf{}. It is a social graph with agents, the
relationships between them, and their privacy policies. Each agent, including
the environment, has their own knowledge base with timestamped pieces of
knowledge. There are two differences compared to \tsnm{}: the shape of formulae
in the users' knowledge bases is different, and their privacy policies use a
different language.

As mentioned before, we will use the original definition for timestamps (Def.
\ref{def:timestamp:t}). Also, \tstampset{} stands for the set of all timestamps
as before, while \frtkbl{} denotes the set of all formulae of the logic
\rtkbl{} defined in the next chapter.

\begin{definition}[\textccrtsnm{}]
\label{def:tsnm:rt}
   Given
   a set of formulae \( \formulae \subseteq \frtkbl{} \),
   a set of privacy policies \polset{},
   and a finite set of agents \( \ag \subseteq \aguni \) from a universe
   \aguni{},
   a \emph{\textrtsnm{}} (\rtsnm{}) is a social graph \( \langle \ag,
   \relstruc, \kb, \\ \vpol \rangle \), where
   \begin{itemize}
      \item
         \ag{} is a nonempty finite set of nodes representing the agents in
         the social network;
      \item
         \relstruc{} is a first-order relational structure over the \rtsnm{},
         consisting of a set of domains \( \{ D_o \}_{o \in \dom{}} \), where
         \dom{} is an index set, and a set of relation symbols, function
         symbols and constant symbols interpreted over a domain;
      \item
         \( \kb: \ag \to 2^{\formulae \times \tstampset} \) is a function
         retrieving the set of accumulated knowledge, each piece with an
         associated timestamp, for each agent, stored in the knowledge base of
         the agent; we write \( \kb_i \) for \( \kb(i) \);
      \item
         \( \pi: \ag \to 2^\polset \) is a function returning the set of
         privacy policies of each agent; we write \( \vpol_i \) for \( \vpol(i)
         \).
   \end{itemize}
\end{definition}

We will use \rtsn{} to denote the set of all \rtsnm{}s.


\subsection{Evolving \osn{}s}
\label{sec:evolving-osns:rt}

In \rtfppf{}, we extend the traces of \tfppf{} with \emph{events} from a
universe \evt{}. What these events look like and how they change the structure
of the \rtsnm{} -- the transition relation -- depends on the \osn{} being
modeled. A \mword{tweet} event could model a tweet being published on Twitter,
for example \cite{ppf}. The meaning of the events is the same as previously
(Sec. \ref{sec:capturing-evolution-of-osn:t}) -- they transform one \rtsnm{} to
another.

\begin{definition}[Trace]
\label{def:trace:rt}
   Given \( \vnat \in \nat \), a trace \( \vtrace \) is a finite sequence
   \[
   \vtrace = \tuple{(\vrtsn_0, \vevts_0, \vts_0), (\vrtsn_1, \vevts_1, \vts_1),
   \dots, (\vrtsn_\vnat, \vevts_\vnat, \vts_\vnat)}
   \]
   such that, for all \( 0 \geq \viter \geq \vnat \), \( \vrtsn_\viter \in
   \rtsn \), \( \vevts_\viter \subseteq \evt \), and \( \vts_\viter \in
   \tstampset \).
\end{definition}

As before, given a trace \vtrace{}, we define \( \tracets = \{ \vts \mid
(\vrtsn, \vevts, \vts) \in \vtrace \} \).

The conditions a trace must fullfil in order to be meaningful
(\emph{well-formed}) closely follow the ones established before for \tfppf{}
(Def. \ref{def:well-formed-trace:t}), with two notable differences.

\paragraph{Accounting for Events}
The definition has to account for events being explicit in the trace. We use an
updated version of the transition relation described before (Sec.
\ref{sec:capturing-evolution-of-osn:t}), this time as \( \trans{} \; \subseteq
\rtsn \times 2^\evt \times \tstampset \times \rtsn \). We have \(
\tuple{\vrtsn_1, \vevs, \vts, \vrtsn_2} \in \; \trans{} \) if \( \vrtsn_2 \) is
the result of the set of events \( \vevs \in \evt \) happening in \( \vrtsn_1
\) at time \vts{}. Note that we allow \vevs{} to be empty, in which case \(
\vrtsn_2 = \vrtsn_1 \). Again, we will use the more compact notation of
\[
   \vrtsn_1 \trans{\vevs, \vts} \vrtsn_2
\]
where appropriate.

\paragraph{Traces without Gaps}
We require that there be a \rtsnm{} in the trace for every time instant between
the beginning of the trace and its end. Formally:

\begin{definition}[Complete Trace]
   Let \vtrace{}  be a trace. \vtrace{} is \emph{complete} if for all \( \vts
   \in \tstampset \) such that \( \min(\tracets) \leq \vts \leq \max(\tracets)
   \), \( \vts \in \tracets \).
\end{definition}

\begin{example}
\label{ex:complete-trace}
   Suppose we have the following trace
   \begin{align*}
      \vtrace = \langle
         &(\vrtsn_0, \; \vevts_0, \; \mts{2016-05-27 00:00:00.000}), \\
         &(\vrtsn_1, \; \vevts_1, \; \mts{2016-05-27 00:00:00.001}), \\
         &(\vrtsn_2, \; \vevts_2, \; \mts{2016-05-27 00:00:00.003})
      \rangle.
   \end{align*}
   \vtrace{} is missing at least one entry to be complete, since there is one
   valid timestamp in the range from \mts{2016-05-27 00:00:00.000} to
   \mts{2016-05-27 00:00:00.003} that is part of \tstampset{} but not
   \tracets{}, and that is \mts{2016-05-27 00:00:00.002}.
\end{example}

Using trace completeness as a prerequisite, we can now provide a formal
definition of well-formed \rtsnm{} traces.

\begin{definition}[Well-Formed Trace]
\label{def:well-formed-trace:rt}
   Let \[ \vtrace = \tuple{(\vrtsn_0, \vevts_0, \vts_0), (\vrtsn_1, \vevts_1,
   \vts_1), \dots, (\vrtsn_\vnat, \vevts_\vnat, \vts_\vnat)} \] be a complete
   trace. \vtrace{} is \emph{well-formed} if the following conditions hold:
   \begin{enumerate}
      \item
         For any \( \vitera, \viterb \) such that \( 0 \geq \vitera, \viterb
         \geq \vnat \) and \( \vitera < \viterb \), it is the case that \(
         \vts_\vitera < \vts_\viterb \).
      \item
         Let \( \kb^\vrtsn \) denote the knowledge base function of model
         \vrtsn{}, and similarly for \( \ag^\vrtsn \). For all \( (\vrtsn,
         \vts) \in \vtrace \), for all \( \vag \in \ag^\vrtsn \), for all \(
         (\vf, \vts_\vf) \in \kb^{\vrtsn}_\vag \), it is the case that \(
         \vts_\vf \leq \vts \).
      \item
         For all \viter{} such that \( 0 \leq \viter \leq \vnat - 1 \), it is
         the case that \( \vrtsn_\viter \trans{\vevs_{i + 1}, \; \vts_{\viter +
         1}} \vrtsn_{\viter + 1} \).
         \label{trans}
   \end{enumerate}
\end{definition}

Again, we will use \traceset{} to refer to the set of all well-formed \rtfppf{}
traces.


\subsection{Notation}
\label{sec:notation:rt}

Accessing a specific \rtsnm{} \vrtsn{} in a well-formed trace is done using the
same notation as before: \( \vtrace[\vts] \) for a timestamp \( \vts \in
\tstampset \). If \( \vts \in \tracets \), then the result is the \rtsnm{} \(
\vrtsn \in \rtsn \) belonging to the tuple \( (\vrtsn, \vevts, \vts') \in
\vtrace \) for which \( \vts' = \vts \), as previously.

There are two corner cases arising from the fact that this time we do not
require that \( \vts \in \tracets \): \vts{} can be out of bounds if it
represents a time instant before or after every \rtsnm{} in the trace. If \(
\vts > \vts' \) for all \( \vts' \in \tracets \), we define \( \vtrace[\vts] \)
to be the very last \rtsnm{} in \vtrace{}. Conversely, if \( \vts < \vts' \)
for all \( \vts' \in \tracets \), then \( \vtrace[\vts] \) will be the very
first \rtsnm{} in \vtrace{}. A demonstration of how the indexing function works
can be found in Example \ref{ex:trace-indexing:rt}.

Given a specific \rtsnm{} \vrtsn{}, we will write \( \ag^\vrtsn \), \(
\relstruc^\vrtsn \), \( \kb^\vrtsn \), \( \Pi^\vrtsn \) to access \vrtsn{}'s
agent set, relational structure, knowledge base, and privacy policy function,
respectively.

\begin{example}
\label{ex:trace-indexing:rt}
   We revisit and expand Example \ref{ex:complete-trace} to illustrate how the
   new indexing function works. Suppose we have the following well-formed \(
   \vtrace \in \traceset \):
   \begin{align*}
      \vtrace = \langle
         &(\vrtsn_0, \; \vevts_0, \; \mts{2016-05-27 00:00:00.000}), \\
         &(\vrtsn_1, \; \vevts_1, \; \mts{2016-05-27 00:00:00.001}), \\
         &(\vrtsn_2, \; \emptyset, \; \mts{2016-05-27 00:00:00.002}), \\
         &(\vrtsn_3, \; \vevts_3, \; \mts{2016-05-27 00:00:00.003}), \\
         &(\vrtsn_4, \; \vevts_4, \; \mts{2016-05-27 00:00:00.004})
      \rangle.
   \end{align*}

   Given \( \vts \in \tracets \), the indexing function returns the
   corresponding \rtsnm{}, as before.
   \begin{align*}
      \vtrace[\mts{2016-05-27 00:00:00.002}] &= \vrtsn_2 \\
      \vtrace[\mts{2016-05-27 00:00:00.004}] &= \vrtsn_4
   \end{align*}

   Given a timestamp \( \vts \notin \tracets \), it returns the closest
   \rtsnm{} in the trace.
   \begin{align*}
      \vtrace[\mts{2006-04-30 20:42}]        &= \vrtsn_0 \\
      \vtrace[\mts{2016-05-26 19:58}]        &= \vrtsn_0 \\
      \vtrace[\mts{2016-05-27 00:00:00.005}] &= \vrtsn_4 \\
      \vtrace[\mts{2016-05-28 00:47}]        &= \vrtsn_4
   \end{align*}
\end{example}
