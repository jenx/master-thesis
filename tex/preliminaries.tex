From the theoretical point of view, the task of designing a time-sensitive
formal framework for \osn{}s based on \fppf{} largely overlaps with the areas
of temporal and epistemic logics. We consider these in Section
\ref{subsec:epistemic-and-real-time-logics}. In Section
\ref{subsec:osn-formalizations}, we discuss frameworks for \osn{}s by other
authors.

Section \ref{sec:fppf} aims to provide a high-level description of \fppf{} as
the foundation for this work.


\section{Theoretical Context}

\subsection{Epistemic, Temporal, and Real-Time Logics}
\label{subsec:epistemic-and-real-time-logics}


First formalizations of what it means to know (or believe) something go back
approximately to the 1950s. Hintikka's \emph{Knowledge and Belief}
\cite{hintikka} is commonly referred to as the first book-length treatment of
the logic of knowledge, or epistemic logic. Since then, epistemic logic found
its applications in many areas, including computer science, security, game
theory, artificial intelligence and economics \cite{dynepi}.

The system proposed by Hintikka in the aforementioned book used the so-called
\emph{possible worlds semantics} (due to Kripke \cite{kripke}), an approach
that would be commonly used in the future to the point where it is often
referred to as the \emph{classical model} \cite{reasoning-survey}. In it,
\emph{agents} operate with possibly incomplete information about their
surroundings: there are properties they are certain about as well as those they
are uncertain about due to lack of knowledge. In the model, a \emph{world} is
essentially a set of facts that hold in a particular version of reality
represented by the world. An agent considers some set of worlds to be possible
-- these are the agent's candidates for what the reality is really like. If
something holds in every world the agent considers possible, the agent
\emph{knows} it for a fact. This is usually written as \( \knows_\vag \vf \),
where \vag{} is the agent in question, \vf{} is a property in the system of
worlds, and \knows{} is the traditional symbol for the epistemic modality. On
the other hand, if there exists at least one possible world in which the
property differs from other possible worlds, the agent does not know which is
the case in reality.

A common example to demonstrate this is depicted in Fig. \ref{fig:worlds}
\cite{reasoning}. We will operate with two agents, Alice and Bob, three
possible worlds \( s_1, s_2, s_3 \), and the primitive proposition \( p \)
meaning ``it is raining in Stockholm''. Each agent \vaga{} considers some
worlds possible, which is captured by an equivalence relation \(
\fancy{K}_\vaga \). A pair of worlds \( (u, v) \) belongs to \( \fancy{K}_\vaga
\) if \vaga{} finds \( v \) possible given the information she or he has in \(
u \).  Each such pair is represented by a directed edge in Fig.
\ref{fig:worlds}. For example, if Alice thinks the world is currently in state
\( s_1 \), then she considers \( s_1, s_2 \) to be possible. If, on the other
hand, she thinks the world is currently in state \( s_3 \), she only considers
\( s_3 \) possible.  In world \( s_1 \) it is not raining in Stockholm (\( \neg
p \) holds), but Alice does not know that, because in world \( s_1 \), she
considers both \( s_1 \), where it is not raining, and \( s_2 \), where it is
raining, possible.  However, in world \( s_3 \), she knows it is raining in
Stockholm (\( \knows_\valice \; p \)) since in all the worlds she considers
possible at \( s_3 \) (in this case, only \( s_3 \) itself), \( p \) holds.

\begin{figure}
\begin{center}
\begin{tikzpicture}
   \tikzstyle{node}=[circle, draw, minimum size=3em, inner sep=3pt]
   \tikzstyle{label}=[font=\small]
   \node[node] (1) at (-3, 0)  {\( \neg p \)};
   \node[node] (2) at (0, 0) {\( p \)};
   \node[node] (3) at (3, 0) {\( p \)};

   \node at (-3.65, -0.65) { \( s_1 \) };
   \node at (0.65, -0.65) { \( s_2 \) };
   \node at (3.65, -0.65) { \( s_3 \) };

   \path[]
      (1) edge [loop above, >=latex] node [label] {Alice, Bob} (1)
      (2) edge [loop above, >=latex] node [label] {Alice, Bob} (2)
      (3) edge [loop above, >=latex] node [label] {Alice, Bob} (3)
      (1) edge [latex-latex] node [label, above] {Alice} (2)
      (2) edge [latex-latex] node [label, above] {Bob} (3);
\end{tikzpicture}
\caption[Simple Kripke structure]
        {A Kripke structure can be modeled by a labeled graph whose nodes
        represent worlds and edges represent the agents' relationships to pairs
        of worlds. More precisely, the worlds \( u, v \) are joined by an edge
        \( a \) whenever agent \( a \) considers world \( v \) possible given
        her information in world \( u \). Such pairs of worlds are said to be
        \emph{indistinguishable} to the agent.}
\label{fig:worlds}
\end{center}
\end{figure}

Though often used in single-agent scenarios to reason about the nature of
knowledge itself, epistemic logic found another major application in
multi-agent systems, with notions such as \emph{distributed} and \emph{common
knowledge} stemming from this combination. In short, \vf{} is distributed
knowledge among a group of agents (\( \disknow_\vgr \vf \)) if \vf{} can be
obtained from their collective knowledge. Common knowledge is a more complex
concept to grasp -- \vf{} is common knowledge among a group \vgr{} of agents
(\( \comknow_\vgr \vf \)) if everyone in \vgr{} knows \vf{} and everyone in
\vgr{} knows that everyone in \vgr{} knows \vf{} and everyone in \vgr{} knows
that everyone in \vgr{} knows that everyone in \vgr{} knows \vf{} and so on
\emph{ad infinitum}. We can take events that happen publicly, with everyone
present and capable of observing the event, as a natural example of common
knowledge -- for instance, two people shaking hands \cite{reasoning-belief}, or
someone making a public proclamation \cite{reasoning}.

Aside from new group epistemic modalities, in a multi-agent setting, one can
also express facts involving the knowledge of several agents at once like
``Alice knows that Bob knows that Charlie does not know that David knows that
it is raining in Gothenburg tonight''. Coming back to the example in Fig.
\ref{fig:worlds}, in world \( s_2 \), Alice knows that Bob knows whether it is
raining in Stockholm (though she does not know what the weather is herself),
since in both \( s_1 \) and \( s_2 \), which are the two worlds Alice considers
possible in \( s_2 \), it is the case that Bob knows what the weather is in the
Swedish capital.  Reasoning about knowledge of multiple agents is crucial from
the point of view of applications in privacy which, in essence, is about
preventing someone from learning something that someone else wants to keep
hidden. This is also one of the reasons why multi-agent epistemic logic is a
natural candidate for reasoning about privacy in \osn{}s.

Epistemic logic is also applicable in a dynamic context in which the world
undergoes a certain evolution. Here, the focus lies on the way knowledge
evolves alongside other properties of the world. A common way to capture the
semantics of this evolution is via \emph{interpreted systems}, where each agent
has a local state whose precise structure depends on the system being modeled.
Moreover, the whole world is characterized by a global state which consists of
the local states of all agents plus the local state of the environment, which
comprises everything relevant not present in the other local states. A
\emph{run}, then, is a function of time returning the global state of the
system at a specific point in time, and a \emph{system} is a set of runs. In a
system, a \emph{point} is characterized by a run and a point in time, and we
say that two points are indistinguishable to an agent if its local state is the
same in both points.  For a simple example, we refer to
\cite{reasoning-belief}.

More generally, formalizing and reasoning about properties in the presence of
time is a field of logic in itself, whose beginnings also date back roughly to
the 1950s. Among the oldest and most well-known temporal logics is LTL (Linear
Temporal Logic) \cite{logic-cs}, originally proposed in \cite{ltl}, which
utilizes two special temporal operators, called ``until'' and ``next''. These
can be used to define the perhaps more well-known box (\al{}) and diamond
(\ev{}), commonly read ``always'' and ``eventually'', respectively. One of the
uses of linear-time logics like LTL is checking whether a system is behaving
correctly. Given two propositions \( p \) and \( q \), one can, for example,
express statements like \( \al(p \to \ev q) \) (``it is always the case that if
\( p \) occurs, then eventually \( q \) will occur'') to ensure that the signal
\( p \) is always followed by the appropriate response in the form of \( q \)
\cite{logics}.

However, pure LTL allows for reasoning about time in a relative, qualitative
way only. It is, for example, not possible to specify the time interval in
which \( q \) has to occur after \( p \), only that it should occur eventually.
This is where other temporal logics, either extensions of LTL, or separate
logics on their own, can be used. These can be classified  based on a number of
attributes, such as the notion of time they use, whether they are \emph{linear}
(only accounting for one evolution of the world) or \emph{branching} (multiple
evolutions), which temporal operators they utilize \cite{logics}, what their
possible axiomatizations are \cite{axiomatizations, axiomatizations2}, or their
properties in terms of expressiveness and complexity \cite{logics-complex}.
Another natural approach to modeling the behavior of real-time systems over
time is to use \emph{timed automata}, as proposed by Alur and Dill
\cite{automata}.

For the purposes of this thesis, in which we aim to present a way to reason
about knowledge of \osn{} users in a linear real-time setting, we are mainly
interested in a combination of the concepts described above, namely real-time
epistemic logics. This particular area contains a number of formalisms created
with a specific application in mind or with the aim to study a specific
property of evolving knowledge \cite{time-bounds, interactive, ignorance,
reactive, timely-common}. In \cite{knowledge-rt}, Wo\'zna and Lomuscio
introduce TCTLKD, a logic to reason about knowledge, correctness and real time
in the context of timed automata and interpreted systems. TCTLKD utilizes the
epistemic modalities \knows{}, \comknow{} and \disknow{} in a standard way (for
instance, an agent is said to know something if it holds in all scenarios it
considers possible). In \cite{agent-time}, Ben-Zvi and Moses revise these
operators themselves by adding a \emph{time instance} to the knowledge
modalities \knows{} (written as \( \knows_{\tuple{a, t}}\) where \( a \) is an
agent and \( t \) is a time instance) and \comknow{}. These represent the
knowledge at a particular moment in time. The authors also introduce a special
predicate for events, \( \mword{occurred}_t(e) \), whose meaning is that the
event \( e \) has occurred by time \( t \).

Though we are unaware of the existence of any real-time epistemic logics
created specifically for the purpose of reasoning about knowledge in evolving
\osn{}s, we draw syntactic inspiration from the aforementioned studies in this
thesis, especially LTL and \cite{agent-time}, which we combine with a
specialized semantics built upon the one used for the original framework
\fppf{} \cite{ppf-14, ppf} (Sec. \ref{sec:fppf}).


\subsection{\osn{} Formalizations}
\label{subsec:osn-formalizations}

There at least three formalizations of \osn{}s whose main focus is on the
privacy of users. Aside from \fppf{}, which we are building upon and to which
we devote the next section, there is also a model for Facebook-like social
network systems by Fong, Anwar and Zhao \cite{facebook} and a framework called
Poporo by Catano, Kostakos and Oakley \cite{poporo}.

In the former (\cite{facebook}), the authors use an access control model to
capture the privacy preservation mechanism of Facebook, which can further be
instantiated into other Facebook-like \osn{}s. They define social network
systems to be made of users and objects (data that can be accessed) owned by
users with the aim to model the authorization mechanism used to grant access to
objects. They also show that the model can be instantiated to be able to
express policies that are currently not supported by Facebook, but are
interesting from the user's perspective.

The Poporo framework \cite{poporo}, on the other hand, consists of several
parts. The main component is called Matelas and is a specification layer built
on predicate calculus. It is used to model the content of a social network
(SN), the privacy policies used, and the friendship relations. The predicate
calculus specification is then turned into a code-level specification model
which the authors call a SN core application. Additional functionalities,
written for example in Java or C, can then be added (plugged in) to the core
and their adherence to the policies stipulated in Matelas can be determined
using a proof validator.

In this thesis, we, too, follow a formal methods-based approach, but one based
on the original \fppf{}.


\section{The First-Order Privacy Policy Framework \fppf{}}
\label{sec:fppf}

Since our extension builds on the foundations laid by \fppf{} \cite{ppf-14,
ppf}, we devote this section to a high-level overview of the framework.

\fppf{} comprises three main parts: (a) a social graph-based model for \osn{}s,
(b) a knowledge-based logic (called \kbl{}) with a satisfiability relation
determining when a formula holds in a network, and (c) a privacy policy
language (\ppl{}) together with a conformance relation defining when a social
network respects a specific policy. In the following we describe each of these
parts in more detail.

\subsection{The Social Network Model \snm{}}

\fppf{} defines a generic model to capture the specifics of any social network
service. This model is based on social graphs, that is, graphs whose nodes
represent users (or, more generally, agents -- we will use these terms
interchangeably), with edges indicating different kinds of relationships
between the users. In the case of \fppf{}, these graphs are enriched with other
components, such as information about the agents' knowledge, stored in their
knowledge bases. The agents' permissions to carry out actions towards other
users, their connections to one another, and their privacy policies are also
included in the model.

\subsection{The Knowledge-Based Logic \texorpdfstring{\kbl{}}{KBL}}

The knowledge-based logic \kbl{} is used to reason about the properties of the
\snm{} and its agents. Built on top of epistemic logic, it utilizes modalities
such as \( \knows_\vag \vf, \everyone_\vgr \vf, \) and \( \someone_\vgr \vf \).
Intuitively, these mean, in turn: agent \vag{} knows \vf{}; every agent in the
set \vgr{} knows \vf{}; and someone in the set \vgr{} knows \vf{}. If, for
instance, Alice can see Bob's location, we can write \( \knows_\valice
\mword{location}(\vbob) \). Connecting this to the \snm{} mentioned before,
this would mean that the piece of information \( \mword{location}(\vbob) \)
either exists explicitly in Alice's knowledge base, or can be derived by
inference from information there.

\kbl{} also directly provides syntactic support for \emph{connections} and
\emph{permissions} (or \emph{actions}) as relationships typical for social
networks. The \emph{friendship} connection on Facebook and the \emph{follower}
connection on Twitter can serve as examples of the former. The action
predicates, on the other hand, model permissions between agents. For instance,
we can express that Bob is not allowed to send a friend request to Alice.

The semantics of \kbl{} is given in the form of the satisfiability relation
which determines whether a \kbl{} formula is valid in a specific social network
model by looking at its properties, such as knowledge of the agents or the
connections and actions between them.

\subsection{The Privacy Policy Language \texorpdfstring{\ppl{}}{PPL}}

The privacy policy language \ppl{} is a formal language that can be used to
write complex privacy policies based on \kbl{}. Each policy belongs to (is
written by) an agent who is regarded as its owner. \ppl{} chooses to write
privacy policies in a restrictive sense, i.e., each privacy policy disallows a
particular behavior to take place or a piece of information to spread. One can
express simple requirements like ``no one can know my location'' or ``Alice
cannot send me a friend request'', but also complex ones, for instance ``if I
create an event and only give certain people the permission to join it, it
cannot be accessed by people outside of the group''.

\ppl{} provides two templates for privacy policies: direct restrictions and
conditional restrictions. In direct restrictions, there is no precondition that
``activates'' the policy; the first two policies mentioned above are examples
of direct restrictions. In conditional restrictions, like the more complicated
event example above, the policy should only be enforced when the model is in a
specific state.

The conformance relation provides the semantics of the privacy policy language,
where the policy is checked to comply with a certain \snm{}. This is done with
the help of \kbl{}'s satisfiability relation.

\subsection{Other Features}

Aside from \snm{}, \kbl{}, and \ppl{}, the framework \fppf{} is also
characterized by a number of additional features. To be able to reason about a
specific \osn{}, it defines the notion of framework instantiation (in
\cite{ppf}, an example instantiation of Twitter is given).

A dynamic version of the framework is also given in \cite{ppf}. Using a labeled
transition system, it is possible to capture certain predecessor-successor
relationships between \snm{}s and the actions that transformed one model to
another. The dynamic behavior of an \osn{} is described using small-step
operational semantics.

\fppf{} also defines what it means for an \osn{} to be privacy-preserving -- no
matter what events happen inside it, it can never violate a user's privacy
policy.
