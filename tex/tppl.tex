One of the main goals of \tfppf{} is to equip users of \osn{}s with additional
power when defining their privacy policies. Building on \fppf{}, this is done
by extending the original privacy policy language \ppl{} with time fields,
which enable the user to specify a (possibly recurring) real-time window in
which their policy should be enforced. The resulting language is called
\texttppl{} (\tppl{}).

In the following sections, we first describe how to capture a specific time
window (or windows) using \tppl{} (Sec.
\ref{sec:privacy-policies-in-real-time:t}), followed by the formal definition,
semantics and properties of \tppl{} (Sec. \ref{sec:writing-privacy-policies:t},
\ref{sec:checking-privacy-policies:t}, \ref{sec:properties-of-tppl:t}). The
remaining Sec. \ref{sec:example-privacy-policies:t} is devoted to examples of
policies written in \tppl{} and their properties and consequences.


\section{Privacy Policies in Real Time}
\label{sec:privacy-policies-in-real-time:t}

\begin{figure}
\begin{center}
\begin{tabular}{ll}
   \( \pol{\neg \alpha}{\vag}^\whenf{start} \) &
   \( \pol{\vfa \then \neg \alpha}{\vag}^\whenf{start} \) \\
   \( \pol{\neg \alpha}{\vag}^\when{start}{duration} \) &
   \( \pol{\vfa \then \neg \alpha}{\vag}^\when{start}{duration} \) \\
   \( \pol{\neg \alpha}{\vag}^\whenrec{start}{duration}{recurrence} \) &
   \( \pol{\vfa \then \neg \alpha}{\vag}^\whenrec{start}{duration}{recurrence} \)
\end{tabular}
\end{center}
\caption[Basic forms of privacy policies in \tppl{}]%
        {Basic forms of privacy policies one can define in \tppl{}. There are
         two basic shapes for the inner content and three possible time
         fields.}
\label{fig:policy-forms:t}
\end{figure}

Our aim is to provide a way to enforce a privacy policy in a specific real-time
window. Additionally, we also want to be able to repeat this window after a
chosen time period has passed. To this end \tppl{} offers a total of three time
fields (Fig. \ref{fig:policy-forms:t}), called \start{}, \duration{}, and
\recurrence{}.

\paragraph{Starting Point}
The first field, \start{}, is a timestamp (Def. \ref{def:timestamp:t}). It is
the only compulsory field out of the three. If a policy only uses the \start{}
field, it is meant to be enforced from the point represented by its value
forward. Such policies (which only have a \start{}) are the \tppl{} version of
the original \ppl{} policies.

\paragraph{Window Duration}
The second field is \duration{}. Unlike the \start{} field, it does not contain
a timestamp, but a slightly different notion with the same name as that of the
field itself: duration (Def. \ref{def:duration:t}). A duration simply stands
for the amount of time between two points in time. If a policy is defined with
both a \start{} field and a \duration{} field, but the last field is missing,
it is meant to be enforced in the time window starting at \start{} and ending
at \( \start + \duration \).

\paragraph{Recurrence Offset}
The last field is \recurrence{} and it also uses the duration format. It can
only be defined if both the \start{} and the \duration{} fields have been
defined. The \recurrence{} field is essentially the offset between the starting
point of each pair of adjacent time windows. A policy with all three fields is
meant to be enforced first in the window from \start{} to \( \start + \duration
\). The second window starts at \( \start + \recurrence \) and ends at \(
\start + \recurrence + \duration \). The third window is offset by twice the
value of the \recurrence{} field, and so on (Table \ref{tab:intervals:t}).

\vspace{1em}

For a more intuitive explanation of how the three fields work together, see
Table \ref{tab:intervals:t} and Fig. \ref{fig:intervals:t}. We also provide a
simple example (Ex. \ref{ex:intervals:t}) at the end of this section.

\begin{figure}
\begin{center}
\begin{tikzpicture}[scale=0.8]
   \tikzstyle{help lines} = [dotted, gray]
   \tikzstyle{diff} = [font=\footnotesize, above=-1, midway]
   \tikzstyle{policy} = [very thick, |-|]

   \draw[style=help lines, step=0.5cm, xshift=-0.0cm] (1, 0) grid (15.49, 2.99);

   \draw[|-stealth] (1, 0) -- (15.5, 0);
   \foreach \x in {1, ..., 29}
      \draw[shift={(\x * 0.5 + 0.5, 0)}, font=\footnotesize] (0, 0) -- (0, -3pt) node[below] {\x};

   \draw[style=policy, teal, |->] (7, 2.5) -- (15.5, 2.5) node[style=diff] {\( \delta_1 \)};
   \draw[style=policy, olive] (2, 1.5) -- (10, 1.5) node[style=diff] {\( \delta_2 \)};
   \draw[style=policy, purple] (3, 0.5) -- (6, 0.5) node[style=diff] {\( \delta_3 \)};
   \draw[style=policy, purple] (8, 0.5) -- (11, 0.5) node[style=diff] {\( \delta_3 \)};
   \draw[style=policy, purple, |->] (13, 0.5) -- (15.5, 0.5) node[style=diff] {\( \delta_3 \)};
\end{tikzpicture}
\caption[Using time fields to define a time window]%
        {Consider three policies \( \delta_1, \delta_2, \delta_3 \), each with
        as many time fields defined as its index. If \( \delta_1 \) only has
        the \start{} field defined and set to \( 13 \), it will hold from that
        point forward, assuming the owner user does not deactivate it
        altogether. If \( \delta_2 \) has a \start{} and a \duration{}, it will
        be enforced in one fixed time window. In this case, the respective
        fields are \( 3 \) and \( 16 \), making the starting time of \(
        \delta_2 \) \( 3 \) and the ending time \( 3 + 16 = 19 \). In case of
        \( \delta_3 \), we also have the \recurrence{} field defined. In this
        case, the values of the fields are, in order, \( 5 \), \( 6 \), and \(
        10 \), meaning that the starting points will be \( 5, 5 + 10, 5 + 2 *
        10, \dots \), while the ending points will be \( 5 + 6, 5 + 10 + 6, 5 +
        2 * 10 + 6, \dots \).}
\label{fig:intervals:t}
\end{center}
\end{figure}

%  \begin{figure}
%  \begin{center}
%  \begin{tikzpicture}[scale=0.8]
%     \tikzstyle{help lines} = [dotted, gray]
%     \tikzstyle{diff} = [font=\footnotesize, above=10pt, midway]
%     \tikzstyle{policy} = [very thick, |-|]

%     \draw[style=help lines, step=0.5cm, xshift=-0.0cm] (1, 0) grid (15.49, 0.99);

%     \draw[|-stealth] (1, 0) -- (15.5, 0);
%     \foreach \x in {1, ..., 22}
%        \draw[shift={(\x * 0.5 + 0.5, 0)}, font=\footnotesize] (0, 0) -- (0, -3pt) node[below] {\x};

%     \draw[style=policy, darkgray] (3, 0.5) -- (6, 0.5) node[style=diff] {};
%     \draw[style=policy, darkgray] (8, 0.5) -- (11, 0.5) node[style=diff] {};
%     \draw[style=policy, darkgray, |->] (13, 0.5) -- (15.5, 0.5) node[style=diff] {};

%     \draw[snake=brace, raise snake=8pt, orange, thick] (3, 0.4) -- (6, 0.4) node[style=diff] {\duration{}};
%     \draw[snake=brace, raise snake=8pt, cyan, thick] (3, 1) -- (8, 1) node[style=diff] {\recurrence{}};
%  \end{tikzpicture}
%  \caption[TODO]%
%          {\( \delta_3 \) from Fig. \ref{fig:intervals:t} revisited in greater
%          detail. TODO}
%  \label{fig:intervals-closeup:t}
%  \end{center}
%  \end{figure}

\begin{table}
\caption[Time windows defined by the time fields of a policy]%
        {Given the time fields \start{}, \duration{}, \recurrence{}, a privacy
        policy should be enforced in intervals \( 0, 1, \dots, \) specified by
        the table. If the \recurrence{} field is not defined, then a policy
        should be only enforced in interval 0. This table is not applicable
        when only the \start{} field has been defined -- in that case, there is
        no end of the time frame.}
\label{tab:intervals:t}
\begin{center}
   \begin{tabular}{lll}
      \toprule
      Interval & Start & End \\
      \midrule
      0 & start                & start + duration \\
      1 & start + recurrence   & start + recurrence + duration \\
      2 & start + 2 recurrence & start + 2 recurrence + duration \\
      3 & start + 3 recurrence & start + 3 recurrence + duration \\
      \( \vdots \) & \( \vdots \) & \( \vdots \) \\
      \viter{} & start + \viter{} recurrence &
      start + \viter{} recurrence + duration \\
      \bottomrule
   \end{tabular}
\end{center}
\end{table}

Let us now formalize the aforementioned notion of \emph{durations}. The
definition provided below is very similar to the definition of timestamps (Def.
\ref{def:timestamp:t}) -- it is also essentially just a natural number
representing a number of milliseconds. In this case, however, it stands for
the absolute difference of two timestamps \( \left| \vts_2 - \vts_1 \right| \),
i.e., the time elapsed between \( \vts_1 \) and \( \vts_2 \).

\begin{definition}[Duration]
\label{def:duration:t}
   A \emph{duration} \vdur{} is a positive natural number representing the
   number of milliseconds elapsed between two points in time.
\end{definition}

As was the case of timestamps, here, too, we will use a more human-readable
format for durations. For instance, \( d = 60 000 \) will be \emph{1 minute},
\( d = 2 167 236 000 \) will be \emph{25 days, 2 hours and 36 seconds}, and so
on. To avoid unnecessary ambiguity, we will avoid referring to months and
years, since their length varies.\footnote{In general, variable month and year
length has a number of consequences from the point of view of practical
usability of \tppl{}; we discuss these in Sec. \ref{sec:properties-of-tppl:t}.}

\begin{example}
\label{ex:intervals:t}
   A privacy policy defined with the time field \[ \whenf{2016-02-02 14:00} \]
   is meant to be enforced from February 2, 2016, 14:00 onwards. If the fields
   defined are \[ \when{2016-02-02 14:00}{6 hours}, \] it gives the policy
   an ending point -- now it should be enforced on February 2, 2016, from 14:00
   to 20:00. And finally, \[ \whenrec{2016-02-02 14:00}{6 hours}{1 day} \]
   stands for every afternoon from 14:00 to 20:00, starting on February 2,
   2016.
\end{example}


\section{Writing Privacy Policies}
\label{sec:writing-privacy-policies:t}

With the intuition behind the time fields explained and the notion of durations
introduced, we are now ready to define the general shape of formulae used in
the \texttppl{} \tppl{}. The language is very similar to that of the original
\ppl{}, as given in \cite{ppf}.

\begin{definition}[Syntax of \tppl{}]
\label{def:syntax-of-tppl:t}
   Given
   agents \( \vaga, \vagb \in \ag \),
   a nonempty set \( \vgr \subseteq \ag \),
   a timestamp \vstart{},
   durations \vdur{}, \vrec{},
   a variable \( x \),
   a formula \( \vf \in \ftkbl \),
   relation symbols \(c_m(\vaga, \vagb), a_n(\vaga, \vagb), \vpred(\vterm)
   \) where \( m \in \conns \) and \( n \in \perms \),
   the \emph{syntax of the \texttppl{}} \tppl{} is inductively defined as:

   \begin{center}
   \begin{tabular}{rcl}
      \( \vpolicy \) & \( ::= \) & \(
         \vpolicy \land \vpolicy \mid
         \forall x.\vpolicy \mid
         \tau \) \\
      \( \tau \) & \( ::= \) & \(
         \pol{\neg \alpha}{\vag}^\whenf{\vstart} \mid
         \pol{\neg \alpha}{\vag}^\when{\vstart}{\vdur} \mid
         \pol{\neg \alpha}{\vag}^\whenrec{\vstart}{\vdur}{\vrec} \mid \) \\
         && \(
         \pol{\vfa \then \neg \alpha}{\vag}^\whenf{\vstart} \mid
         \pol{\vfa \then \neg \alpha}{\vag}^\when{\vstart}{\vdur} \mid
         \pol{\vfa \then \neg \alpha}{\vag}^\whenrec{\vstart}{\vdur}{\vrec} \) \\
      \( \alpha \) & \( ::= \) & \(
         \alpha \land \alpha \mid
         \forall x.\alpha \mid
         \vfb \mid
         \gamma\prime \) \\
      \( \gamma\prime \) & \( ::= \) & \(
         \knows_\vag \gamma \mid
         \learns_\vag \gamma \mid
         \disknow_\vgr \gamma \mid
         \comknow_\vgr \gamma \) \\
      \( \gamma \) & \( ::= \) & \(
         \gamma \land \gamma \mid
         \neg \gamma \mid
         \vpred(\vterm) \mid
         \gamma' \mid
         \vfb \mid
         \forall x. \gamma \) \\
      \( \vfb \) & \( ::= \) & \(
         c_m(\vaga, \vagb) \mid
         a_n(\vaga, \vagb) \) \\
   \end{tabular}
   \end{center}
\end{definition}

Furthermore, let \ftppl{} denote the set of all formulae of \tppl{} and
\ftpplr{} the set of all formulae created using the \( \alpha \) category (the
restrictions).


\section{Checking Privacy Policies}
\label{sec:checking-privacy-policies:t}

Now that we are able to define policies to be enforced in real-time, we need to
be able to determine whether they actually work in an \osn{}. For this purpose
we formalize the semantics of \tppl{}, which define whether a privacy policy is
or is not violated during the evolution of an \osn{}, encoded in a well-formed
trace.

The actual checking process uses notions defined earlier: it combines basic
trace operations (Sec. \ref{sec:notation:t}) with the satisfiability relation
for \tkbl{} (Def. \ref{def:satisfiability-relation:t}).

\begin{definition}[Conformance Relation]
\label{def:conformance-relation:t}
   Given
   a well-formed trace \( \vtrace \in \traceset \),
   an agent \( \vag \in \ag \),
   formulae \( \vf \in \ftkbl, \alpha \in \ftpplr \),
   and \( \vpolicy, \vpolicy_1, \vpolicy_2 \in \ftppl \),
   the conformance relation \con{} is defined as shown in Fig.
   \ref{fig:conformance-relation:t}.
\end{definition}

\begin{figure}
\begin{center}
\begin{tabular}{lcl}
   \( \vtrace \con \vpolicy_1 \land \vpolicy_2 \) &
   iff &
   \( \vtrace \con \vpolicy_1 \land \vtrace \con \vpolicy_2 \) \\
   \( \vtrace \con \forall x. \vpolicy \) &
   iff &
   for all \( v \in D_o^{\vtrace[\vts]} \), \( \vtrace \con \vpolicy[v / x] \) \\
   \\
   \( \vtrace \con \pol{\neg \alpha}{\vag}^\whenrec{\vstart}{\vdur}{\vrec} \) &
   iff &
   for all \( c \in \nat^0 \) \\ && such that
   \( 0 \leq \vstart + c\vrec \leq
   \max(\tracets) \),\\&&
   \( \vtrace \con \pol{\neg \alpha}{\vag}^\when{\vstart{} +
   c\vrec{}}{\vdur} \) \\
   \( \vtrace \con \pol{\neg \alpha}{\vag}^\when{\vstart}{\vdur} \) &
   iff &
   \( \vtrace\slice{\vstart}{\vstart + \vdur}, \vstart \sat \al(\neg \alpha) \) \\
   \( \vtrace \con \pol{\neg \alpha}{\vag}^\whenf{\vstart} \) &
   iff &
   \( \vtrace\slice{\vstart}{}, \vstart \sat \al(\neg \alpha) \)\\
   \\
   \( \vtrace \con \pol{\vfa \then \neg \alpha}{\vag}^\whenrec{\vstart}{\vdur}{\vrec} \) &
   iff &
   for all \( c \in \nat^0 \) \\ && such that
   \( 0 \leq \vstart + c\vrec \leq
   \max(\tracets) \),\\&&
   \( \vtrace \con \pol{\vfa \then \neg
   \alpha}{\vag}^\when{\vstart{} + c\vrec}{\vdur} \) \\
   \( \vtrace \con \pol{\vfa \then \neg \alpha}{\vag}^\when{\vstart}{\vdur} \) &
   iff &
   \( \vtrace\slice{\vstart}{\vstart{} + \vdur}, \vstart \sat \al(\vfa \then \neg \alpha) \) \\
   \( \vtrace \con \pol{\vfa \then \neg \alpha}{\vag}^\whenf{\vstart} \) &
   iff &
   \( \vtrace\slice{\vstart}{}, \vstart \sat \al(\vfa \then \neg \alpha) \) \\
\end{tabular}
\end{center}
\caption[Conformance relation for \tppl{}]%
        {The semantics of the \texttppl{} \tppl{} is given in terms of the
        conformance relation \con{}. We use \( \nat^0 \) to denote the set of
        all natural numbers and zero. Both the direct and the conditional
        restriction policy form have two base cases, based on the time fields
        defined. These base cases take the relevant subtrace of the original
        trace and the content of the policy, with \al{} attached, is then
        checked using the satisfiability relation.}
\label{fig:conformance-relation:t}
\end{figure}

The conformance relation treats both direct and conditional restrictions in the
same way: both are interpreted using the satisfiability relation based on the
time fields they have; the shape of the content of the policy is not relevant.
The three cases mirror the three possible configurations of policy time fields:
by Def. \ref{def:syntax-of-tppl:t}, a policy either only has a \start{}, or it
has a \start{} and a \duration{}, or a \start{}, a \duration{} and a
\recurrence{}.

\paragraph{One-Field Policies}
This is a base case in the conformance relation. With only the \start{} field
defined, we take the suffix of the original trace \vtrace{} starting at
\start{}: since we are only interested in enforcing the policy from that point
forward, we only need to look at those \tsnm{}s in the trace whose timestamps
are \start{} or greater. We also set the trace position variable to \start{}.
Finally, we attach the \al{} (\emph{always}) temporal operator to the policy,
to make sure that it will be checked in every \tsnm{} of the subtrace, and we
delegate the checking process to the satisfiability relation.

\paragraph{Two-Field Policies}
This is also a base case in the conformance relation. It is very similar to the
previous one, the only difference being that we do not delegate a whole suffix
of the original trace, but a subtrace starting at \start{} and ending at \(
\start + \duration \). The philosophy behind this is exactly the same as
before: we are only interested in the time window between these two time
points.

\paragraph{Three-Field Policies}
This is the only complex form. The intuition behind the reduction to a simpler
case is illustrated by Fig. \ref{fig:intervals:t}, which was mentioned before.
Essentially, we cut the original trace into the windows specified by the time
fields and check each of the windows separately by reducing to a simpler case.
This is quite straightforward, since we can easily calculate the starting and
ending point of any time window from the three fields, as shown previously by
Table \ref{tab:intervals:t}.

Note that even though the counter \( c \) (which basically stands for the
window number) is a nonnegative integer, we only check a finite number of
windows (it would not make much sense otherwise, since the trace itself is
finite). There is also one notable corner case: if the last window is not
complete (because the trace ends somewhere in it), we just check the part that
can be accessed. This is because of the way the subtrace function (Sec.
\ref{subsubsec:subtraces:t}) works when handling timestamps out of range.


\section{Clarifications}
\label{sec:properties-of-tppl:t}

\subsection{Checking Outside Policy Windows}
\label{subsec:checking-outside-policy-windows:t}

The behavior and meaning of \tppl{} policies might differ from initial
expectations. For example, imagine Alice wants to define a policy that says
that Bob cannot learn about any of her pictures during the weekend.

Now let us say that on Wednesday, Bob learns about Alice's picture, taken at a
party on Saturday.  Is this a violation of Alice's policy? Not in terms of the
conformance relation. This is because the relation trims away any part of the
trace that does not have a weekend timestamp, so the policy is really checked
only in the time frame it is defined for. Essentially, it says that Bob is not
allowed to learn about Alice's picture during any weekend.

To further clarify what the policy actually means, consider now the opposite
scenario: on Saturday, Bob learns about Alice's picture, which was taken on
Wednesday. This \emph{is} a violation of Alice's policy, because the
conformance (and satisfiability, by extension) relation does not care about the
original date of something happening or appearing in the \osn{}, it only checks
whether something was learned at a specific point (e.g. on Saturday).

For similar potentially ambiguous cases and their handling in \tppl{}, we refer
to the examples in Sec. \ref{sec:example-privacy-policies:t}.

\subsection{Variable Window Offset}
\label{subsec:variable-window-offset:t}

Shortly after defining the notion of durations, we briefly mentioned that we
will avoid using months and years in the human-readable format to avoid
confusion caused by the variability in their length. Take month lengths, for
example: a month can mean any number of days from 28 to 31, depending on a
number of factors. In year length, leap years are the cause of ambiguity.

It might seem that this is not actually a problem; we can just use the precise
number of days in a specific month or year to translate a duration
unambiguously. The issue, however, extends further.

Imagine a user wants to define a policy that should be enforced on the last day
of each month. There is no way to achieve this with just one policy with all
three time fields: as the \recurrence{} field value is used as the offset
between the starting point of windows, the duration between each window will be
the same and there is no way to offset the second window by 31 days, the next
one by 28 days, and so on.

Nevertheless, there is a number of solutions that can be used in scenarios like
these. For example, one can define multiple policies (or one longer policy that
is a conjunction of several basic policies) with the same content, but
different time fields. Since the number of options one needs to account for is
finite, the number of policies needed to capture them is finite as well.
Alternatively, we could have a notion of months and years that would be
automatically translated to the right number of days based on the starting
point of the policy window. For example, if we are currently checking a policy
in a time window starting on November 5, 2016, and the next window based on the
\recurrence{} field is supposed to be a month from that point, we could
calculate the number of days between November 5 and December 5 and use that as
the next starting point.


\section{Example Privacy Policies}
\label{sec:example-privacy-policies:t}

\begin{example}
\label{ex:supervisor}
   On Friday, April 15, 2016, Alice decides that she wants to keep her private
   life separate from her life as a graduate student. In an \osn{} with privacy
   policies using \tppl{}, she can keep her supervisor Bob from learning her
   location on weekends by defining the following privacy policy:
   \[
      \vpolicy = \pol{
         \neg \learns_\vbob \mword{location}(\valice)
      }{\valice}^\whenrec{2016-04-16}{2 days}{1 week}
   \]
   Given a trace of the \osn{} Alice and Bob use, \vpolicy{} would be checked
   first in the subtrace from Saturday 16th, 00:00, to Monday 18th, 00:00, then
   again from Saturday 23rd, 00:00, to the end of Sunday 24th, and so on.

   In order for the trace to be in conformance with \vpolicy{}, in each of
   these subtraces, the \tkbl{} formula \( \al \left(\neg \learns_\vbob
   \mword{location}(\valice)\right) \) needs to be satisfied. Based on the
   satisfiability relation (Def. \ref{def:satisfiability-relation:t}), this is
   only the case if the formula \( \neg \learns_\vbob \mword{location}(\valice)
   \) is satisfied at every point of the subtrace. This, in turn, means that it
   must not be the case that \( \learns_\vbob \mword{location}(\valice) \) is
   satisfied in any of the \tsnm{}s of the subtrace.

   To determine whether \( \learns_\vbob \mword{location}(\valice) \), we
   consult the closure of Bob's knowledge base to see whether it contains the
   pair \( (\mword{location}(\valice), \vts) \), where \vts{} is the timestamp
   of the \tsnm{} currently being checked. In other words, we have to determine
   whether Bob learned (either directly, or by inference) \(
   \mword{location}(\valice) \) at point \vts{}. As \vts{} is a time in one of
   the time windows \vpolicy{} is defined for (i.e., a weekend sometime after
   April 16th), Bob having access to this particular piece of knowledge would
   be a violation of Alice's policy, since it would mean Bob managed to learn
   Alice's location on a weekend. Note, however, that Bob learning Alice's
   weekend location at any point \emph{not} during a weekend is not considered
   a violation of the policy. This is due to the fact that the policy is not
   checked outside the time windows it is defined for.

   There is also an alternative way of writing the policy using the knowledge
   modality \knows{}. In fact, although it is not always so, in this simple
   case \learns{} and \knows{} are interchangeable. Consider the policy
   \[
      \vpolicy' = \pol{
         \neg \knows_\vbob \mword{location}(\valice)
      }{\valice}^\whenrec{2016-04-16}{2 days}{1 week}
   \]
   and the way it is checked with respect to a trace, according to the
   conformance and satisfiability relations. The process is the same as before
   -- we slice the trace into relevant windows and delegate the actual checking
   of these subtraces to the sastifiability relation -- until the point when we
   are looking for a certain piece of information in the closure of Bob's
   knowledge base in every relevant model of the subtrace. While in the
   previous case we were looking for a ``fresh'' \( \mword{location}(\valice)
   \) (that is, one with the same timestamp as that of its model), this time we
   are interested in any \( \mword{location}(\valice) \) whose timestamp falls
   anywhere within the start of the time window being checked and the timestamp
   of the model. In order for such \( \mword{location}(\valice) \) to be in the
   closure of Bob's knowledge base, he must have learned it sometime in the
   time frame being checked, which is exactly what the original policy
   \vpolicy{} meant.

   Note that when checking a specific time frame starting at time \vstart{},
   there might be instances of \( \mword{location}(\valice) \) with timestamps
   older than \vstart{} in Bob's knowledge base. This, however, does not
   violate \( \vpolicy' \) since by Def. \ref{def:conformance-relation:t}, we
   are only checking the subtrace starting at \vstart{}, and by Def.
   \ref{def:satisfiability-relation:t}, in order for Bob to know something,
   that particular piece of knowledge has to have a timestamp greater than or
   equal to the oldest timestamp of the trace, which in this case is \vstart{},
   so older knowledge is not taken into account.
\end{example}

\begin{example}
   Charlie will start a new one-month job on July 1st, 2016, and she would like
   to ensure that, during this period, only her friends can learn about her
   posts when she is at home, and only her colleagues can learn about her posts
   when she is at work. In \tppl{}, she can accomplish this by defining two
   privacy policies
   \(
   \vpolicy_1, \vpolicy_2 \):
   \begin{align*}
      \vpolicy_1 = \forall x. &\pol{
         \mword{home}(\vcharlie) \land \neg \mword{friends}(\vcharlie, x)
         \then \\ &\;\;\neg \learns_x \mword{post}(\vcharlie, \mword{text})
      }{\vcharlie}^\whenf{2016-07-01, 31 days} \\
      \\
      \vpolicy_2 = \forall x. &\pol{
         \mword{working}(\vcharlie) \land \neg \mword{colleagues}(\vcharlie, x)
         \then \\ &\;\;\neg \learns_x \mword{post}(\vcharlie, \mword{text})
      }{\vcharlie}^\whenf{2016-07-01, 31 days}
   \end{align*}
   The predicates \( \mword{home}(\vcharlie) \) and \(
   \mword{working}(\vcharlie) \) are checked by consulting the environment,
   which contains everything that holds in a specific model. Checking whether
   \( \vpolicy_1 \) or \( \vpolicy_2 \) are violated ultimately boils down to
   checking whether all \tsnm{}s in the trace, starting at 2016-07-01 00:00 and
   ending at 2016-08-01 00:00, satisfy the whole formula inside the policy. It
   should be noted, however, that the time when the post was actually posted is
   irrelevant; what matters is when the users learn about it. So if Charlie is
   working and her colleague Daniel somehow gains access to her post that was
   originally posted when she was at home, it is not a violation of either of
   the policies.

   In Example \ref{ex:supervisor}, the learning and knowledge modalities could
   be swapped without changing the meaning of the policy. This is usually not
   possible in conditional policies like \( \vpolicy_1 \) and \( \vpolicy_2 \).
   Consider the following variation of a simplified \( \vpolicy_1 \):
   \[
      \vpolicy_1' = \forall x. \pol{
         \neg \mword{friends}(\vcharlie, x) \then \neg \knows_x
         \mword{post}(\vcharlie, \mword{text})
      }{\vcharlie}^\whenf{2016-07-01, 31 days}
   \]
   Imagine we want to check \( \vpolicy_1' \) on a trace with a user Greg whose
   relationship with Charlie is a little complicated. They first meet and
   become friends on July 15th, but for some reason Greg unfriends Charlie on
   July 25th. Both dates fall into the time window of the policy.

   Now let us say Greg learns Charlie's location when they are friends, on July
   20th. Since agents do not forget anything, it will still be in his knowledge
   base after he unfriends Charlie. This, however, means that \( \vpolicy_1' \)
   will be violated, since at any point after July 25th, someone who is not a
   friend of Charlie's knows her location with a timestamp greater than July
   1st.

   If we use the learning modality instead, this will not be a problem. The
   difference is that in order for \( \vpolicy_1' \) to be violated, the state
   of Charlie and Greg not being friends and Greg \emph{knowing} Charlie's
   location have to happen at the same time (in the same model). If \learns{}
   is used instead of \knows{}, the violating condition changes to not being
   friends and Greg \emph{learning} Charlie's location at the same time.
\end{example}

\begin{example}
   Another example of a privacy policy could be ``if we break up, then you can
   no longer learn about pictures I am tagged in''. Let us say it is Frank who
   wants to enforce this and Eve is his current girlfriend. He is free to write
   the following in \tppl{}:
   \[
      \vpolicy = \pol{
         \mword{brokenUp}(\mword{Frank}, \mword{Eve}) \land
         \mword{taggedIn}(\mword{Frank}, \eta) \then \neg \learns_{\mword{Eve}}
         \mword{picture}(\eta)
      }{\mword{Frank}}^\whenf{\vts}
   \]
   To rephrase the policy to better capture the meaning of the individual
   predicates, we could say: ``If Frank and Eve are in the state of being broken
   up according to the environment and Frank is tagged in picture \( \eta \),
   then Eve does not learn about the picture \( \eta \)'', where \( \eta \) is
   a unique identifier tying the \emph{taggedIn} and \emph{picture} predicates
   together.

   Here, too, checking whether \vpolicy{} is violated with respect to a trace
   means checking the contents of the policy in all \tsnm{}s in the trace,
   starting at time \vts{}. So if Eve gains access to a picture Frank is tagged
   in that is new to her (no matter when it was originally posted) when they
   are no longer together, Frank's policy will be violated.
\end{example}
