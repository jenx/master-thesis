Both \tfppf{} and \rtfppf{} have their strengths and weaknesses. Both allow for
fine-grained policies and even though \tfppf{} is in many aspects limited
compared to \rtfppf{}, it carries out its specialized task of enabling policies
in fixed, recurring windows \emph{ad infinitum} well. \tkbl{} acts as an
important milestone from the original \kbl{} by allowing to reason about time,
though it is arguably \rtkbl{} that actually achieves what one would consider
reasoning about knowledge in real time. However, it is still our belief that
there is a large number of use cases in which \tfppf{} would be a good
candidate, especially if one does not need the additional machinery introduced
in \rtfppf{}, or if one wants to define truly indefinitely recurrent policies.
\rtfppf{}, on the other hand, is preferable where even more fine-grained
policies are needed.

\tfppf{} has a number of areas to improve, many of which \rtfppf{} manages to
address -- after all, they were the reason why \rtfppf{} was developed. Most of
these issues stem from the limited expressive power of \tkbl{}, which
introduces time, but in a relative way only. This is because we utilize the
\al{} and \ev{} operators without any notion of real time, so while it is
possible to ensure, for example, that something will eventually hold, one
cannot specify the actual time when it should. This is partially remedied on
the \tppl{} level, but again only in a limited way -- while policies can be
defined for fixed time windows, they are very rigid and incapable of carrying
out any more complex requests a user might have. They only specify a part of
the trace where a property should hold; anything outside is not taken into
account and even the ability to navigate the subtrace is limited.

While \rtfppf{} manages to address most of these issues, it comes with its own
set of shortcomings. The definition of the trace indexing function (Sec.
\ref{sec:notation:rt}) is problematic in its handling of out-of-bounds
timestamps. To recapitulate, if one tries to access a \rtsnm{} with a timestamp
that is greater than the last one in the trace, the function just returns the
last one, and analogously for a timestamp lower than the very first. This can
be an issue if one looks at the definition of the satisfiability relation (Def.
\ref{def:satisfiability-relation:rt}) -- when quantifying over timestamps, it
treats them like any other variable, but when evaluating a basic formula, the
timestamp in it has a very special interpretation and is used to navigate the
trace. So when interpreting a predicate like \( \mword{location}(\valice,
\mts{2000-01-01}) \) over a trace that starts at time \( \mts{2016-01-01} \),
we actually end up checking something entirely different, \(
\mword{location}(\valice, \mts{2016-01-01}) \). Probably the most reasonable
solution is that accessing an out-of-bounds \rtsnm{} should be undefined, since
we cannot say anything about what the \osn{} looked like at that point. This,
however, has other implications, as it essentially results in a three-valued
logic.

As hinted at in the previous paragraph, timestamps are really a very specific
part of the \rtkbl{} syntax. The timestamp domain is naturally ordered and they
receive special treatment in the satisfiability relation. These are good
reasons for promoting the timestamp domain to a special one, but in this thesis
we just assume they have the same status and importance as other domains.

Also, looking back at the \rtkbl{} satisfiability relation, we mentioned that
the timestamp on the left-hand side is only used in one case -- to deal with
quantification. This is, actually, the reason why we introduced this special
timestamp in the first place: to be able to determine which relational
structure to use when looking for candidates for a certain variable. This
choice has its issues, as does the \tkbl{} satisfiability relation (Def.
\ref{def:satisfiability-relation:t}), since it uses the same principle. The
problem is that the structure of the \rtsnm{}s in the trace changes over time
-- for example, users may leave the \osn{}. However, when quantifying over the
domain of agents, we take the set of agents at time \vts{}, when the quantified
formula is being checked, which may result in problems when evaluating any
inner formulae at a different point in time.

As we mentioned in the chapter about \rtppl{} (Chap. \ref{chapter:rtppl}), the
language has some disadvantages, some even compared to \tppl{}. Unlimited
recurrence is hard to achieve unless the user does not mind activating a new
policy everytime it exhausts the finite number of time windows it can capture.
This could be remedied by adding arithmetic to \rtppl{}.


\section{Future Work}
\label{sec:future-work}

We can imagine a large number of potential paths future research can take to
improve the frameworks we propose in this work. Some of them have been hinted
at in the previous section. Other ideas are:

\begin{itemize}
   \item
      We mentioned early on that the formalization of \osn{}
      \emph{instantiation} and \emph{privacy preservation} are out of the scope
      of this work. These two notions are natural candidates for future
      investigation.
   \item
      Some information and knowledge can be overriden by new data of the same
      kind. Consider, for example, knowing someone's birthday and knowing their
      location. While the location of an agent may and probably will change a
      lot, their birthday is set in stone. If Alice learns Bob's location on
      Monday afternoon, it does not tell her anything about his location a year
      from then, but learning Bob's birthday is permanent. We could therefore
      find a categorization for information, for instance into \emph{transient}
      and \emph{permanent}. This would bring the framework closer to the actual
      notion of learning and knowing in reality.
   \item
      A related notion is that of \emph{knowledge validity in time}. Each piece
      of information has an implicit timeout -- the time after which it is no
      longer up-to-date. For permanent knowledge, like someone's birthday, this
      would be infinity. However, the real challenge would be to reasonably
      determine this timeout for transient knowledge. For example, how long
      should someone's location be valid for? Probably not until they post a
      new one, because the two may be years apart. This is an interesting
      question both philosophically and from the privacy point of view, which
      could be explored in future work.
   \item
      When we talk about knowledge in this work, we actually mean \emph{belief}
      as there is no universal way to check whether an arbitrary piece of
      information one comes across on an \osn{} is true. One problem that stems
      from this is that an agent can receive conflicting pieces of information.
      For example, Alice might tag Bob in a post about sitting in a pub in
      Dublin with her friends, but at the same time, Bob might post a picture
      of himself in front of the Eiffel Tower. Which of these is true? Is any?
      It might worthwhile to have a subset of data one is sure of and can
      really claim to know, like birthdays of family members or information
      about events one has actually attended. This would help separate belief
      from actual knowledge.
   \item
      It might also be interesting to explore other \emph{formalizations of
      real-time}, since here we model it discretely. It seems to work well for
      our purpose, but other formalizations might bring other advantages.
\end{itemize}
