Since \rtkbl{} treats timestamps like any other variable, we do not need to
define any special time fields to take care of the time aspect artificially, as
we did previously. Instead, we rely on the power of \rtkbl{} itself.


\section{Writing Privacy Policies}

The definition of the \textrtppl{} (\rtppl{}) contains less rules than that for
\tppl{}. It closely follows the description of the original \fppf{}, adding
timestamps where necessary.

\begin{definition}[Syntax of \rtppl{}]
\label{def:syntax-of-rtppl:rt}
   Given
   agents \( \vaga, \vagb \in \ag \),
   a nonempty set of agents \( G \subseteq \ag \),
   timestamps \( \vstart, \vts \),
   a variable \( x \),
   relation symbols \( c_m(\vaga, \vagb, \vts), a_n(\vaga, \vagb, \vts) \),
   \( \vpred(\vterm, \vts) \),
   and a formula \( \vfa \in \frtkbl \),
   the \emph{syntax of the \textrtppl{}} \rtppl{} is inductively defined as:

   \begin{center}
   \begin{tabular}{rcl}
      \( \vpolicy \) & \( ::= \) & \(
         \vpolicy \land \vpolicy \mid
         \forall x. \vpolicy \mid
         \pol{\neg \alpha}{\vag}^\vstart \mid
         \pol{\vfa \then \neg \alpha}{\vag}^\vstart
      \) \\
      \( \alpha \) & \( ::= \) & \(
         \alpha \land \alpha \mid
         \forall x. \alpha \mid
         \exists x. \alpha \mid
         \vfb \mid
         \vfc'
      \) \\
      \( \vfb \) & \( ::= \) & \(
         c_m(\vaga, \vagb, \vts) \mid
         a_n(\vaga, \vagb, \vts)
      \) \\
      \( \vfc' \) & \( ::= \) & \(
         \knows_\vag^\vts \vfc \mid
         \disknow_\vgr^\vts \vfc \mid
         \comknow_\vgr^\vts \vfc
      \) \\
      \( \vfc \) & \( ::= \) & \(
         \vfc \land \vfc \mid
         \neg \vfc \mid
         \vpred(\vterm, \vts) \mid
         \vfc' \mid
         \vfb \mid
         \forall x. \vfc
      \) \\
   \end{tabular}
   \end{center}
\end{definition}

We will use \frtppl{} to denote the set of all privacy policies created
according to the previous definition.


\section{Checking Privacy Policies}

To determine whether a policy gets violated in an evolving \osn{}, we formalize
the notion of conformance for \rtppl{}.

\begin{definition}[Conformance Relation]
\label{def:conformance-relation:rt}
   Given
   a well-formed trace \( \vtrace \in \traceset \),
   a variable \( x \),
   a timestamp \( \vstart \),
   and an agent \( \vag \in \ag \),
   the \emph{conformance relation \con{}} is defined as shown in Fig.
   \ref{fig:conformance-relation:rt}.
\end{definition}

\begin{figure}
\begin{center}
\begin{tabular}{lcl}
   \( \vtrace \con \forall x. \vpolicy \) &
   iff &
   for all \( v \in D_o \), \( \vtrace \con \vpolicy[v / x] \) \\
   \\
   \( \vtrace \con \pol{\neg \alpha}{\vag}^\vstart \) &
   iff &
   \( \vtrace \sat \neg \alpha \) \\
   \( \vtrace \con \pol{\vfa \then \neg \alpha}{\vag}^\vstart \) &
   iff &
   \( \vtrace \sat \vfa \then \neg \alpha \) \\
\end{tabular}
\end{center}
\caption[Conformance relation for \rtppl{}]%
        {The semantics of the \textrtppl{} \rtppl{} is given in terms of the
         conformance relation \con{}.}
\label{fig:conformance-relation:rt}
\end{figure}

The definition is quite simple, especially compared to that of conformance of
\tppl{} (Def. \ref{def:conformance-relation:t}). If the policy is quantified,
we substitute in the usual way. The main body of the policy in double brackets
is dealt with by simply delegating to the satisfiability relation.


\section{Clarifications}

\subsection{Indefinitely Recurring Windows}

While \rtppl{} allows users to define more fine-grained policies than \tppl{}
(Sec. \ref{sec:rtppl-examples}), it has its limits. For example, defining a
recurring privacy policy cannot be done without help of specialized predicates,
and some more complex recurrent windows cannot be defined at all.

Suppose a user wants to define a policy which utilizes a timestamp that should
represent a weekend. We can introduce a specialized predicate for this, called
\( \mword{weekend}(\vts) \), which is true if \vts{} belongs to a weekend.
Analogously, we could have predicates like \mword{weekday}, \mword{evening}, or
\mword{Monday}, that would help anchor a timestamp in correct context.

But how does one deal with contexts that are more arbitrary, for example, from
a fixed date to a fixed date each year? This is not easily done. Though we can
compare timestamps -- so it is possible to write statements like \( \vts <
\mts{2016-28-05} \land \vts > \mts{2016-02-03} \) --, there is no other way to
capture multiple windows (like ``from February 3 to May 28 every year'') than
to restrict the timestamps by adding more intervals into the conjunction, which
is not ideal, especially when the interval should repeat more often and many
times -- perhaps even indefinitely.

\subsection{Restricting in the Past}

The ability to use timestamps in almost arbitrary ways in \rtppl{} creates a
problem that has to do with restricting past behavior.
Consider the following policy:
\[
   \vpolicy = \pol{\neg \knows_\vagb^1 \vf}{\vag}^4
\]
Here, \vag{} is attempting to prevent \vagb{} from learning \vf{} at time 1,
but the activation time of the policy is 4. In other words, \( \vpolicy \)
wants to restrict something that may have already happened, in which case it
would be trivially violated.


\section{Examples}
\label{sec:rtppl-examples}

\begin{example}
   Revisiting Example \ref{ex:supervisor}, assume Alice decides to hide all her
   weekend locations from her supervisor Bob. She has a number of options how
   to achieve this, depending on what the precise meaning of the policy should
   be.

   If the idea she has is to restrict Bob learning her weekend location
   directly when she posts it, she can define
   \[
      \vpolicy_1 = \forall x. \pol{
         \mword{weekend}(x) \then \neg \knows_\vbob^x \mword{location}(\valice,
         x)
      }{\valice}^\mts{2016-04-16}
   \]
   where the \mword{weekend} predicate is true if the timestamp supplied
   represents a time during a weekend. This policy can be read as ``if \( x \)
   is a time instant during a weekend, then Bob is not allowed to learn at \( x
   \) Alice's location from time \( x \)''.

   This, however, is a very specialized scenario that captures only a small
   number of situations. Bob is, for example, free to learn Alice's location at
   any point not during the weekend, or at any point during the weekend when
   Alice's location is no longer up-to-date.  Though there might be scenarios
   where this might be the desired behavior, we can define a policy that seems
   much closer to the intuitive meaning of learning someone's location on a
   weekend. Consider
   \[
      \vpolicy_2 = \forall x. \pol{
         \mword{weekend}(x) \then \neg \exists y.(\knows_\vbob^y
         \mword{location(\valice, x))}
      }{\valice}^\mts{2016-04-16}.
   \]
   Here, Bob is not allowed to learn Alice's location from a weekend, no matter
   when. If the policy does not get violated, then Alice's weekend locations
   will be completely safe from Bob -- on the \osn{}, at least.
\end{example}

\begin{example}
   One of the advantages of \rtppl{} is the ability to distinguish between the
   original time of a piece of information and the time when it should be
   hidden. Suppose Diane activates the following policy:
   \[
      \vpolicy_1 = \forall x. \forall y. \pol{
         \neg \mword{friends}(\vdiane, x, y) \then \neg
         \exists z.(\knows_x^y \mword{post}(\vdiane, z))
      }{\vdiane}^\mts{2016-05-28}
   \]
   This aims to prevent anyone who is not a friend of Diane's from learning any
   of her posts (here we assume that the \mword{friends} connection is
   reflexive for simplicity, otherwise the restriction would target Diane
   herself, too).

   Though \( \vpolicy_1 \) may seem reasonable enough, it might be
   unnecessarily restrictive. Let us say there is another user, Ethan. Diane
   becomes friends with Ethan on May 31, so when her policy is already in
   effect. Should Ethan be able to learn about Diane's posts from when they
   were not friends? Not according to \( \vpolicy_1 \), which says that no one,
   regardless of their relationship with Diane at the moment, is able to learn
   about her posts from when they were not friends.

   Note that while this may indeed be the desired behavior, it is, for example,
   not what happens on Facebook, where when two users become friends, they are
   free to access each other's timeline including past events and posts.
   \rtppl{} is expressive enough to model this behavior as well. We can define:
   \begin{align*}
      \vpolicy_2 = \forall x. \forall y. \forall y'. \pol{
         &\neg \mword{friends}(\vdiane, x, y) \land \neg
         \mword{friends}(\vdiane, x, y') \then \\
         &\;\;\; \neg \knows_x^{y'} \mword{post}(\vdiane, y)
      }{\vdiane}^\mts{2016-05-28}
   \end{align*}
   This policy precisely defines the point in time \emph{from} when to hide
   information, \( y \), as well as the point in time \emph{when} to hide it,
   \( y' \). It says, ``if Diane is not friends with someone, then that someone
   cannot learn her posts, but only if they come from a time when they were not
   friends''. Note that \( \vpolicy_2 \) says nothing about users who are
   currently friends of Diane's, which is different from \( \vpolicy_1 \) --
   here her friends can learn anything, including past posts from when they
   were not friends with her.
\end{example}
