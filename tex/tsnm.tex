\section[\textcctfppf{} \tfppf{}]{\textcctfppf{} \\ \tfppf{}}

As an extension of \fppf{}, the framework proposed in this part relies on the
foundations laid by the original framework. As a result, the structure of the
framework proposed in this part is very similar to the original one. We, too,
define three major components upon which the framework is built: (a) a
\texttsnm{} (\tsnm{}), together with the notion of traces, that is, sequences
of these models that represent the evolution of an \osn{}; (b) a \texttkbl{}
(\tkbl{}) with temporal modalities, inspired by temporal logics such as LTL,
and one new epistemic modality; and (c) a \texttppl{} (\tppl{}), enabling the
user to define a (possibly recurring) real-time window in which their policy
should be enforced.

Together, these parts form the new \texttfppf{} (\tfppf{}). Its formal
definition follows.

\begin{definition}[\textcctfppf{}]
\label{def:tfppf:t}
   The tuple \[ \tfppf{} = \tuple{\tsn, \tkbl{}, \sat, \tppl{}, \con} \]
   is a \emph{\texttfppf{}} where
   \begin{itemize}
      \item
         \tsn{} is the set of all possible \textpltsnm{};
      \item
         \tkbl{} is a \texttkbl{};
      \item
         \sat{} is a satisfiability relation defined for \tkbl{};
      \item
         \tppl{} is a formal \texttppl{};
      \item
         \con{} is a conformance relation defined for \tppl{}.
   \end{itemize}
\end{definition}

In what follows we devote a chapter to each of these components. We first
formalize \textpltsnm{} in the remaining sections of this chapter. In Chapter
\ref{chapter:tkbl}, we give the syntax and the semantics (\sat{}) of the
\texttkbl{} \tkbl{}. Finally, we describe the \texttppl{} \tppl{} together with
its conformance relation \con{} in Chapter \ref{chapter:tppl}.


\section{\textcctsnm{} \tsnm{}}

In the original framework \fppf{}, social network models were defined as social
graphs with agents, their knowledge bases and privacy policies, and a
first-order relational structure. \tfppf{} retains this definition, but extends
the formulae in the knowledge bases of agents with a timestamp, thus rooting
each piece of information in time. What notion of time we will be working with
in this thesis is discussed and formalized in Section
\ref{sec:formalizing-time:t}.

Moreover, in addition to the definition of \textpltsnm{}, we also introduce our
notion of dynamic, evolving \osn{}s. In \fppf{}, a labeled transition system
was used for a similar purpose. Here, we use sequences of \textpltsnm{}, each
representing a snapshot of the modeled \osn{} at a specific moment in time.  We
describe and formalize these notions in Section
\ref{sec:capturing-evolution-of-osn:t}.


\subsection{Formalizing Time}
\label{sec:formalizing-time:t}

Adding timestamps to formulae in the knowledge bases of agents enables us to
tell apart pieces of information in a way that is not possible in the original
framework.  Let us take the simple example of Alice learning Bob's location. In
\fppf{}, the simplest way to capture this scenario is that the predicate \(
\mword{location}(\mword{Bob}) \) either exists explicitly in Alice's knowledge
base, or she is able to infer it from the knowledge she already has. At a later
point, Alice might learn the location of Bob again, and again it will be
available to her as \( \mword{location}(\mword{Bob}) \). As a consequence,
Alice knowing Bob's location does not really tell us anything about Bob's
location -- the information might easily be outdated and there is no way to
find out.

Another option in \fppf{} is to somehow establish the relative order of the
instances when Alice learns Bob's location using so-called resource
identifiers, so she would be able to access predicates like \(
\mword{location}(\mword{Bob}, 1) \) or \( \mword{location}(\mword{Bob}, 47) \).
This is closer to the design of \tfppf{}, as it enables us to ``refresh''
knowledge without losing previous instances.  Still, however, although it is
possible to determine the relative order of pieces of knowledge of the same
kind, there is nothing preventing the latest one from being outdated, as in the
previous case.

By adding timestamps to the agents' knowledge bases, one is both able to tell
apart pieces of information of the same kind, and precisely pinpoint the moment
when the piece of knowledge was learned.

Let us now formalize the notion of timestamps.

\begin{definition}[Timestamp]
\label{def:timestamp:t}
   A \emph{timestamp} \vts{} is a natural number representing the number of
   milliseconds elapsed since January 1, 1970, 00:00:00.000.
\end{definition}

Of course, we could have chosen a large number of equally good starting points;
the beginning of 1970 was chosen simply because it is also the Unix epoch.

We will use \tstampset{} to denote the set of all timestamps.

\begin{table}
\caption[Human-readable timestamp format]%
        {When talking about timestamps, we use the standard human-readable ISO
        format, optionally skipping some parts of the time component. We use
        the format in the first column.}
\label{tab:human-readable-timestamp-format:t}
\begin{center}
\begin{tabular}{ll}
   \toprule
   Compact & Full \\
   \midrule
   2016-03-26 10:59:08.562 & 2016-03-26 10:59:08.562 \\
   2000-01-01              & 2000-01-01 00:00:00.000 \\
   1990-03-10 12:00        & 1990-03-10 12:00:00.000 \\
   8080-01-12 23:59:02.100 & 8080-01-12 23:59:02.100 \\
   \bottomrule
\end{tabular}
\end{center}
\end{table}

Since referring to specific dates as, for example, \( \vts{}_1 = 1458986348693
\) or \( \vts{}_2 = 192814041542693 \) has the potential to become rather
confusing, we will use the more human-readable standard ISO format \cite{iso}
of \verb+YYYY-MM-DD+ \verb+hh:mm:ss.sss+ when talking about
timestamps.\footnote{Note that there is a number of technical issues that would
arise if this format was to be used in practice. For instance, we do not
specify whether we count leap seconds -- which would have an effect on the
conversion from and to the ISO format --, or what timezone we are in.}
Optionally we might skip the time part, in which case we assume it defaults to
00:00:00.000, or its suffix, starting with either seconds or milliseconds --
then we assume the number of missing (milli)seconds amounts to zero. Table
\ref{tab:human-readable-timestamp-format:t} contains a number of examples. In
some cases where we want to demontrate a point (most often in examples) and
specific timestamps are not important, we will use dummy timestamps 1, 2, and
so on.

There are two main reasons behind choosing this particular timestamp format.
First, it is practical -- any timestamp represents a valid date and time and
one does not have to consider technicalities such as variable month and year
length. The other reason is that they work well in contexts where they are
meant to be used -- both attached to pieces of knowledge in agents' knowledge
bases, and in the privacy policy language.

Their use in the knowledge bases relies on the basic property that, given any
two timestamps \( \vts{}_1 \) and \( \vts{}_2 \), it is possible to determine
their relative order, i.e., considering the two points in time represented by
the timestamps, determine which one happened sooner (later). As for their use
in privacy policies, one of the goals of the extension in this part is to allow
users to define a real-time window frame in which their privacy policy should
be enforced. Therefore, when defining the time-sensitive version of the privacy
policy language (\tppl{}), the user has to be able to pinpoint a specific
moment in time, which is done using our notion of timestamps. In addition, this
simple definition makes it possible to not only determine the order of
timestamps, but also to quantify the distance \( \left| \vts{}_2 - \vts{}_1
\right| \), which is later formalized as \emph{duration} (Def.
\ref{def:duration:t}). This property is also used in \tppl{}, where the most
basic privacy policy window is defined using a timestamp and a duration field
(as opposed to using two timestamps).


\subsection{Capturing the Evolution of an \osn{}}
\label{sec:capturing-evolution-of-osn:t}

We now provide a definition of a \texttsnm{}, with timestamps attached to
information in the knowledge bases of agents. \ftkbl{} stands for the set of
all well-formed formulae of the time-sensitive knowledge-based logic \tkbl{},
which is defined at a later point (Def. \ref{def:syntax-of-tkbl:t}). The
specific shape of the formulae used is not important at this point.

\begin{definition}[\textcctsnm{}]
\label{def:tsnm:t}
   Given
   a set of formulae \( \formulae \subseteq \ftkbl{} \),
   a set of privacy policies \polset{},
   and a finite set of agents \( \ag \subseteq \aguni \) from a universe
   \aguni{},
   a \emph{\texttsnm{}} (\tsnm{}) is a social graph \tuple{\ag, \relstruc, \kb,
   \vpol}, where
   \begin{itemize}
      \item
         \ag{} is a nonempty finite set of nodes representing the agents in
         the \osn{};
      \item
         \relstruc{} is a first-order relational structure over the \tsnm{},
         consisting of a set of domains \( \{ D_o \}_{o \in \dom} \), where
         \dom{} is an index set; a set of relation symbols, function
         symbols and constant symbols interpreted over a domain;
      \item
         \( \kb: \ag \to 2^{\formulae \times \tstampset} \) is a function
         retrieving the set of accumulated knowledge, each piece with an
         associated timestamp, for each agent, stored in the knowledge base of
         the agent; we write \( \kb_i \) for \( \kb(i) \);
      \item
         \( \pi: \ag \to 2^\polset \) is a function returning the set of
         privacy policies of each agent; we write \( \vpol_i \) for \( \vpol(i)
         \).
   \end{itemize}
\end{definition}

We denote \tsn{} the set of all \tsnm{}s.

Let us take a closer look at each component.

\paragraph{Agents}
We assume that, in addition to ``normal'' agents (that is, those who represent
actual users of the \osn{}), there is also a special agent called the
\emph{environment} (\environ). The environment contains all knowledge that is
true in the TSNM.

\paragraph{Knowledge Bases}
The set retrieved by the knowledge base function \kb{} of an agent contains
everything the agent has learned so far, written in the language of the
\texttkbl{} \tkbl{}. This can be anything from simple predicates meaning ``I
learned Alice's location on April 29, 2016 at 18:44:13.562'', to more complex
information such as ``on July 15, 2008 at 10:00 I learned that Bob learned that
Alice knew Bob's location''. Note that there is only one timestamp in any
formula in an agent's knowledge base, so timestamps are not nested -- they
always refer to the formula as a whole. For the formalization of what agents
can know and what it means in the context of \tfppf{} we refer to Chapter
\ref{chapter:tkbl}, which is devoted to \tkbl{} and its properties.

\paragraph{The First-Order Relational Structure}
The overall shape of \relstruc{} depends on the properties of the \osn{} being
modeled. This is especially apparent in the case of relational symbols, which
are used to represent the \emph{connections} and \emph{permission actions} (or
just \emph{permissions}, or \emph{actions}) -- the edges of the underlying
social graph. Connections stand for relationships (not necessarily symmetric)
between agents, such as \emph{friends} (usually two-way) or \emph{follower}
(usually one-way). Permissions model what actions a user is allowed to execute
toward other users. For example, Alice might not give Bob permission to send
her a friend request.

More formally, given sets of indices \conns{} for connections and \perms{}
for permissions, we define connections and permissions as families of binary
relations on the set of agents: \( \confam \subseteq \ag \times \ag \) and \(
\perfam \subseteq \ag \times \ag \), respectively. For better readability, we
will use a predicate like \( \friends{}(\valice, \vbob) \) to mean that \(
\valice, \vbob \in \ag \) belong to the binary relation \friends{}.

\paragraph{Privacy Policies}
In addition to possessing a set of knowledge, each agent is able to define
their own set of privacy policies using the \texttppl{} \tppl{}. Generally
speaking, the goal of these policies is to restrict the audience of something
in the \osn{}, for example a post or a picture. The language itself and its
attributes are described in detail in Chapter \ref{chapter:tppl}.

\begin{figure}
\begin{center}
\begin{tikzpicture}
   \tikzstyle{node}=[circle, draw, minimum size=2em]
   \tikzstyle{label}=[font=\small]
   \node[node] (a) at (0, 2)  {};
   \node[node] (b) at (-3, 0) {};
   \node[node] (c) at (3, 0) {};

   \node at (0, 2.65) { Alice };
   \node at (-3.65, -0.65) { Bob };
   \node at (3.65, -0.65) { Charlie };

   \path[]
      (a) edge [latex-latex, bend right=10] node [label, sloped, anchor=south] {\emph{friends}} (b)
      (c) edge [-latex, bend right=10, dashed] node [label, sloped, anchor=south] {\emph{friendRequest}} (a)
      (b) edge [-latex, bend right=20] node [label, below] {\emph{blocked}} (c);
\end{tikzpicture}
\caption[Simple social graph]
        {A simple social graph with three agents, two connections (the
        bidirectional \emph{friends} and the one-way \emph{blocked}) and one
        permission (\emph{friendRequest}). The connections are represented by
        normal edges, the permission uses a dashed one. The information from
        this graph can be summarized as follows: Alice is friends with Bob and
        vice-versa, Bob is blocking Charlie, and Charlie is allowed to send a
        friend request to Alice.}
\label{fig:tsnm}
\end{center}
\end{figure}

\begin{example}
\label{ex:tsnm}
   We give a simple example of a \tsnm{} in Fig. \ref{fig:tsnm} with \( \ag =
   \{ \valice, \) \( \vbob, \vcharlie \} \), \( \conns = \{ \mword{friends},
   \mword{blocked} \} \) and \( \perms = \{ \mword{friendRequest} \} \). The
   agents' knowledge bases and privacy policies are not depicted at this point
   -- we will revisit this example later once we have established the general
   shape of formulae in the users' knowledge bases.
\end{example}

Since \tfppf{} is by definition a dynamic framework, we need a way to capture
the evolution of an \osn{}. This is done by using sequences of \tsnm{}s, so
that every \tsnm{} in the sequence represents a snapshot of the \osn{} at some
point. This structure is called a \emph{trace}.

More specifically, a trace is a sequence of pairs consisting of a specific \(
\vtsn{} \in \tsn{} \) together with a timestamp \vts{}. The intuitive meaning
is that each such \vtsn{} is a snapshot of the \osn{} at point \vts{}, as if we
froze the network, along with the knowledge and relationships between its
agents, at that moment.

We demand traces be finite. This makes working with them more practical,
especially in terms of the semantics we give at a later point (Def.
\ref{def:satisfiability-relation:t}), which relies on access to all social
network models in the trace.

\begin{definition}[Trace]
\label{def:trace:t}
   Given \( \vnat \in \nat \), a trace \( \vtrace \) is a finite sequence
   \[
   \vtrace = \tuple{(\vtsn_0, \vts_0), (\vtsn_1, \vts_1), \dots, (\vtsn_\vnat,
   \vts_\vnat)}
   \]
   such that, for all \( 0 \geq \viter \geq \vnat \), \(
   \vtsn_\viter \in \tsn \) and \( \vts_\viter \in \tstampset \).
\end{definition}

This basic definition does not impose any restrictions on the timestamps or
\tsnm{}s used, so even sequences of \tsnm{}s that have little in common, with
arbitrary, and potentially repeating, timestamps, are traces by definition. To
single out meaningful traces, that is, those that actually capture the
evolution of an \osn{}, we introduce the notion of \emph{well-formed traces}. 
To be well-formed, a trace has to satisfy three conditions.

\paragraph{Order}
We place a restriction on the ordering of the pairs in the trace with regards
to the timestamps, which we require to be strictly ordered from smallest to
largest. This allows us to immediately identify the successors and predecessors
and the gradual changes happening between \tsnm{}s at different positions of
the trace.

\paragraph{Plausible Knowledge}
Moreover, for each \( (\vtsn, \vts) \) in the trace, the timestamps used inside
the agents' knowledge bases must be at most \vts{}. Intuitively, if a snapshot
of an \osn{} was taken at time \vts{}, then \vts{} should be the latest point
at which the agents could have obtained new knowledge.

\paragraph{Continuity}
Finally, for the successor-predecessor relationships between two adjacent
\tsnm{}s to make sense, each \tsnm{}, starting from the second one, has to be
the result of some events happening in the \tsnm{} that comes immediately
before. For this purpose we use the transition relation \trans{} defined for
\fppf{} \cite{ppf}, extended with a timestamp capturing when a particular set
of events happens. More formally, assuming that \evt{} is the set of all
events, the \trans{} relation here is characterized as \( \trans{} \; \subseteq
\tsn \times 2^\evt \times \tstampset \times \tsn \) and the tuple \(
\tuple{\vtsn_1, \vevs, \vts, \vtsn_2} \) is in \trans{} if \( \vtsn_2 \) is the
result of the nonempty set of events \vevs{} happening in \( \vtsn_1 \) at time
\vts{}. We will write this as \( \vtsn_1 \trans{\vevts, \vts} \vtsn_2 \).

\begin{definition}[Well-Formed Trace]
\label{def:well-formed-trace:t}
   Let \[ \vtrace = \tuple{(\vtsn_0, \vts_0), (\vtsn_1, \vts_1), \dots,
   (\vtsn_\vnat, \vts_\vnat)} \] be a trace. \vtrace{} is \emph{well-formed} if
   the following conditions hold:
   \begin{enumerate}
      \item
         For any \( \vitera, \viterb \) such that \( 0 \geq \vitera, \viterb
         \geq \vnat \) and \( \vitera < \viterb \), it is the case that \(
         \vts_\vitera < \vts_\viterb \).
      \item
         Let \( \kb^\vtsn \) denote the knowledge base function of model
         \vtsn{}, and similarly for \( \ag^\vtsn \). For all \( (\vtsn, \vts)
         \in \vtrace \), for all \( \vag \in \ag^\vtsn \), for all \( (\vf,
         \vts_\vf) \in \kb^{\vtsn}_\vag \), it is the case that \( \vts_\vf
         \leq \vts \).
      \item
         For all \viter{} such that \( 0 \leq \viter \leq \vnat - 1 \), it is
         the case that \( \vtsn_\viter \trans{\vevs, \; \vts_{\viter + 1}}
         \vtsn_{\viter + 1} \), where \( \vevs \subseteq \evt \) and \vevs{} is
         nonempty.
   \end{enumerate}
\end{definition}

We will use \traceset{} to refer to the set of all well-formed traces.


\subsection{Notation}
\label{sec:notation:t}

In the previous text we established a number of interconnected notions. In
order to simplify notation used in the future, we introduce the following
shortcuts. We assume \vtrace{} is a well-formed trace.

\subsubsection{Trace Properties}
\label{subsec:trace-properties:t}

We name the set of all timestamps associated with the \tsnm{}s in the trace
\tracets{}. In other words, \( \tracets = \{ \vts \mid (\vtsn, \vts) \in
\vtrace \} \). In a similar manner, we use \tracetsns{} to denote the set of
all \tsnm{}s in the trace: \( \tracetsns = \{ \vtsn \mid (\vtsn, \vts) \in
\vtrace \} \).

\begin{example}
\label{ex:trace-properties:t}
   Let us say that
   \begin{align*}
      \vtrace = \langle &(\vtsn_0, \; \mts{2016-04-30 19:57}),\\
                        &(\vtsn_1, \; \mts{2016-04-30 19:59}),\\
                        &(\vtsn_2, \; \mts{2016-04-30 20:00}),\\
                        &(\vtsn_3, \; \mts{2016-04-30 20:37}),\\
                        &(\vtsn_4, \; \mts{2016-04-30 20:47})\rangle.
   \end{align*}
   Retrieving the set of all timestamps or \tsnm{}s present in \vtrace{} is
   straightforward:
   \begin{align*}
      \tracets = \{&\mts{2016-04-30 19:57},
                    \mts{2016-04-30 19:59},
                    \mts{2016-04-30 20:00},\\
                   &\mts{2016-04-30 20:37},
                    \mts{2016-04-30 20:47}
                 \}\\
      \tracetsns = \{&\vtsn_0, \vtsn_1, \vtsn_2, \vtsn_3, \vtsn_4 \}
   \end{align*}
\end{example}

\subsubsection{Accessing Parts of a Trace}
\label{subsec:accessing-parts-of-trace:t}

\paragraph{Specific \tsnm{}s}

Often we will need to refer to a specific \tsnm{} in a trace. For this purpose,
\( \vtrace[\vts] \) for a timestamp \( \vts \in \tracets \) is the model \(
\vtsn{} \in \tsn{} \) belonging to the pair \( (\vtsn, \vts_{\vtsn}) \in
\vtrace \) for which \( \vts_{\vtsn} = \vts \). Note that in a well-formed
trace, there is exactly one such model.

Once we have retrieved a specific \tsnm{} \( \vtsn \in \tracetsns \), we will
often need to refer to its components directly. We will write \( \ag^\vtsn \),
\( \relstruc^\vtsn \), \( \kb^\vtsn \), \( \Pi^\vtsn \) to access \vtsn{}'s
agent set, relational structure, knowledge base, and privacy policy function,
respectively.

\begin{example}
\label{ex:specific-tsnms:t}
   Let \vtrace{} be the trace from Example \ref{ex:trace-properties:t}. The
   indexing function can be used to access any of the three models in a
   straightforward way:
   \begin{align*}
      \vtrace[\mts{2016-04-30 19:57}] &= \vtsn_0 \\
      \vtrace[\mts{2016-04-30 19:59}] &= \vtsn_1 \\
      \vtrace[\mts{2016-04-30 20:00}] &= \vtsn_2
   \end{align*}
   Note that by definition, only timestamps that actually exist in the trace
   (that is, those that belong to \tracets{}) can be used, so for example \(
   \vtrace[\mts{2016-04-30 19:58}] \) is invalid.

   If we want to get the knowledge base function of \( \vtsn_1 \), we can write
   \[ \kb^{\vtrace[\mts{2016-04-30 19:59}]} \] and similarly for other
   \tsnm{}s and their components.
\end{example}

\paragraph{Subtraces}
\label{subsubsec:subtraces:t}

We define \( \vtrace\slice{\vts_1}{\vts_2} \), where \( \vts_1 \leq \vts_2 \),
to be a function returning a specific subtrace of \vtrace{}. The first element
of the subtrace is the first \( (\vtsn, \vts) \in \vtrace \) for which \( \vts
\geq \vts_1 \); the last element of the subtrace is the last \( (\vtsn, \vts)
\in \vtrace \) such that \( \vts \leq \vts_2 \). Essentially, the function
simply extracts all the \tsnm{}s with timestamps that fall into the interval
from \( \vts_1 \) to \( \vts_2 \), inclusive.

Note that unlike the indexing function \( \vtrace[\vts] \), here the index
timestamps need not be actual timestamps of the models in the trace. It might
even be the case that \( \vts_1 \) is greater than the last timestamp of
\vtrace{} (using the notation introduced previously, \( \vts_1 \geq
\max(\tracets ) \)) or that \( \vts_2 \) is less than the very first timestamp
of \vtrace{} (\( \vts_2 \leq \min(\tracets ) \)). In these cases, the subtrace
returned is empty.

We will also use \( \vtrace\slice{\vts}{} \) and \( \vtrace\slice{}{\vts} \)
to retrieve the suffix (prefix) of \vtrace{} that satisfies the corresponding
part of the previous description.

\begin{example}
\label{ex:subtraces:t}
   Once again, we will use \vtrace{} from the previous Examples
   \ref{ex:trace-properties:t} and \ref{ex:specific-tsnms:t}. Imagine we want
   to take the subtrace starting with \( \vtsn_2 \) and ending with \( \vtsn_4
   \).  There is a number ways to achieve this. For instance:
   \begin{align*}
      \vtrace&\slice{\mts{2016-04-30 20:00}}{\mts{2016-04-30 20:47}}\\
      \vtrace&\slice{\mts{2016-04-30 19:59:30.587}}{\mts{2016-05-01}}\\
      \vtrace&\slice{\mts{2016-04-30 19:59:11}}{}
   \end{align*}
\end{example}
